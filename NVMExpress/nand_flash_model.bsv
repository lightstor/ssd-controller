/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

/*
module name: NAND Flash Model
author name: Maximilian Singh
email id: maximilian.singh@student.kit.edu
last update done on 9th June 2014

This module provides a NAND Flash Model which can be connected to the NVM
Controller and simulates a NAND Flash + Controller.  It supports only the
commands which are used by the NVM Controller and saves the data in BRAMs.
*/

/* global parameter definitions */
`include "global_parameters"

package nand_flash_model;

import BRAM::*;
import DReg::*;
import Vector::*;
import global_definitions::*;


(* synthesize *)
//(* always_ready *)
//(* always_enabled *)
module mkNand_flash_model(Ifc_nand_flash_model);

Reg#(bit) rg_enable <- mkReg(0);  // high active enable
Reg#(bit) drg_interrupt <- mkDReg(0);
Reg#(UInt#(TAdd#(TAdd#(TLog#(1024), 11), 1))) rg_read_count <- mkReg(0);
Reg#(UInt#(TAdd#(TLog#(1024), 11))) rg_write_count <- mkReg(0);
Reg#(UInt#(64)) rg_read_addr <- mkReg(0);
Reg#(UInt#(11)) rg_read_length <- mkReg(0);


BRAM1Port#(Bit#(TAdd#(`MEM_ADDR_SIZE, TLog#(TDiv#(TMul#(4096, 8), `WDC)))), Bit#(`WDC)) nand_mem <-
	mkBRAM1Server(defaultValue);

method Action _request_data(UInt#(64) _address, UInt#(11) _length) if
		(rg_enable == 1'b1 && rg_read_count == 0);
	/* Read request. */
	$display("%d: nand_flash_model: _request_data from 0x%x", $stime(), _address);
	nand_mem.portA.request.put(BRAMRequest {
		write: False,
		responseOnWrite: False,
		address: pack(_address)[`MEM_ADDR_SIZE - 1:0] << valueOf(TLog#(TDiv#(TMul#(4096, 8), `WDC))),
		datain: ?
	});
	rg_read_addr <= _address;
	rg_read_length <= _length;
	rg_read_count <= rg_read_count + 1;
	drg_interrupt <= 1'b1;
endmethod

method ActionValue#(Bit#(`WDC)) _get_data_() if (rg_enable == 1'b1 && rg_read_count != 0);
	/* Read register. */
	if (rg_read_count == extend(rg_read_length) * fromInteger(valueOf(TDiv#(TMul#(4096, 8), `WDC)))) begin
		rg_read_count <= 0;
	end else begin
		nand_mem.portA.request.put(BRAMRequest {
			write: False,
			responseOnWrite: False,
			address: (pack(rg_read_addr)[`MEM_ADDR_SIZE - 1:0] << valueOf(TLog#(TDiv#(TMul#(4096, 8), `WDC)))) + truncate(pack(rg_read_count)),
			datain: ?
		});
		rg_read_count <= rg_read_count + 1;
	end
	let lv_response <- nand_mem.portA.response.get();
	return lv_response;
endmethod: _get_data_

method Action _write(UInt#(64) _address, Bit#(`WDC) _data, UInt#(11) _length) if
		(rg_enable == 1'b1);
	/* Write register. */
	$display("%d: nand_flash_model: write 0x%x to 0x%x (%d)", $stime(), _data, _address, rg_write_count);
	nand_mem.portA.request.put(BRAMRequest {
		write: True,
		responseOnWrite: False,
		address: (pack(_address)[`MEM_ADDR_SIZE - 1:0] << valueOf(TLog#(TDiv#(TMul#(4096, 8), `WDC)))) + truncate(pack(rg_write_count)),
		datain: _data
	});
	if (rg_write_count == extend(_length) * fromInteger(valueOf(TDiv#(TMul#(4096, 8), `WDC))) - 1) begin
		rg_write_count <= 0;
	end else begin
		rg_write_count <= rg_write_count + 1;
	end
endmethod: _write

method Action _enable(bit _nand_ce_l);
	/* Enable or disable the NAND Controller.

	TODO can this be a timing problem because it is not enabled before the
	next cycle
	TODO what happens if it is disabled while busy?
	*/
	rg_enable <= ~_nand_ce_l;
endmethod: _enable

method bit interrupt_() if (rg_enable == 1'b1);
	/* Interrupt when read data is ready in buffer. */
	return drg_interrupt;
endmethod: interrupt_

method bit busy_() if (rg_enable == 1'b1);
	/* Return busy state of the NAND Controller. */
	return 1'b0;
endmethod: busy_

endmodule: mkNand_flash_model
endpackage: nand_flash_model
