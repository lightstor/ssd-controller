package FtlProcessor;

import global_definitions::*;
import FIFO::*;
import FIFOF::*;
import SpecialFIFOs::*;
import BRAMFIFO::*;
import ConfigReg::*;

interface Ifc_ftl_in;
   method Action _put_cmd(Ftl_cmd ftl_cmd);
   method bit put_cmd_busy();
   method Action _put_prp(UInt#(64) main_mem_addr);
   method bit put_prp_busy();
endinterface

interface Ifc_ftl_out;
   method ActionValue#(Nand_cmd) get_cmd();
   method ActionValue#(Ftl_prp_tag) get_prp();
endinterface

interface Ifc_ftl_processor;
   interface Ifc_ftl_in ifc_ftl_in;
   interface Ifc_ftl_out ifc_ftl_out;
endinterface

// TODO TODO move the lba and pba to cmd interface. Unnecessary
// address calcuation and buffer space wastage
module mkFtlProcessor(Ifc_ftl_processor);
   Wire#(bit) dwr_cmd_busy <- mkDWire(1'b1);
   Wire#(bit) dwr_request_busy <- mkDWire(1'b1);
   FIFOF#(Ftl_cmd) ff_ftl_cmd <- mkSizedFIFOF(5);
   FIFOF#(UInt#(64)) bff_ftl_req <- mkSizedBRAMFIFOF(513);
   // TODO TODO unnecessary bit usage
   Reg#(Bit#(16)) rg_request_counter <- mkConfigReg(0);
   Wire#(Nand_cmd) wr_send_ftl_cmd <- mkWire();
      
   interface Ifc_ftl_in ifc_ftl_in;
      method Action _put_cmd (Ftl_cmd ftl_cmd);
	 $display ("%d: FTLProcessor : enqueued ftl command",$stime());
	 ff_ftl_cmd.enq(ftl_cmd);
      endmethod
      
      method bit put_cmd_busy();
	 return pack(!ff_ftl_cmd.notFull());
      endmethod
      
      method Action _put_prp (UInt#(64) _main_mem_addr);
	 $display("%d: FTLProcessor: enqueued ftl request",$stime());
	 bff_ftl_req.enq(_main_mem_addr);
      endmethod
      
      method bit put_prp_busy();
	 return pack(!bff_ftl_req.notFull());
      endmethod
   endinterface
   
   interface Ifc_ftl_out ifc_ftl_out;
   
      // This method implicitly fires when wr_send_ftl_cmd is written i.e., for the first transfer.
      // This requires that get_map and get_cmd to be called simulataneously otherwise it will not work.
   
      method ActionValue#(Nand_cmd) get_cmd();
	 $display ("%d: FtlProcessor: Sent cmd to nvme",$stime());
	 return wr_send_ftl_cmd;
      endmethod
   
      method ActionValue#(Ftl_prp_tag) get_prp();
   
	 $display ("%d: NvmController: Sent map to nvme: (%h--%h)",$stime(),
		   bff_ftl_req.first(),ff_ftl_cmd.first().nand_lba);
	 if (rg_request_counter == 0)
	    begin
	       wr_send_ftl_cmd <= Nand_cmd {
		  opcode	: ff_ftl_cmd.first().opcode,
		  length	: ff_ftl_cmd.first().length,
		  
		  nand_pba	: ff_ftl_cmd.first().nand_lba
		  };
	    end
	 $display ("%d: NvmController: during get_map rg_request_counter: %d ff_ftl_cmd.first(): (%d--%d)",$stime(),rg_request_counter,ff_ftl_cmd.first().opcode, ff_ftl_cmd.first().length);
	 Maybe#(Bit#(32)) lv_last_transfer;
	 if (rg_request_counter == ff_ftl_cmd.first().length)
	    begin
	       rg_request_counter <= 0;
	       ff_ftl_cmd.deq();
	       lv_last_transfer = tagged Valid ff_ftl_cmd.first().tag;
	    end
	 else
	    begin
	       rg_request_counter <= rg_request_counter + 1;
	       lv_last_transfer = tagged Invalid;
	    end
	 bff_ftl_req.deq();
	 return Ftl_prp_tag {
	    main_mem_addr : bff_ftl_req.first(),
	    tag           : lv_last_transfer
	    };
      endmethod
   endinterface
   
endmodule

endpackage