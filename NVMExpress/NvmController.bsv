/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
--
-- Copyright (c) 2013,2014  Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/
////////////////////////////////////////////////////////////////////////////////
// Name of the Module		: NVM Express Controller
// Coded by
//				: M.Shanmukh Abhishek (original version)
//				: Maximilian Singh (Restructure and Redesign, new version)
//				: Keerthi Kiran H Pujar (Multi-Channel aspect of NVMe
//				: Controller, original version)
//
// Module Description		: This module contains the high levelmodules of the
//							NVMexpress Controller.
//
//Functionality at a glance	: 
//			                1. Command Fetch
//			                2. Command Arbitration
//			                3. ASQ and ISQ Command Execution
//			                4. Command Completion
//			                5. Status update
//			                6. Error Logs
//                                      7. Attachable ftl interface
//			                8. NAND Flash Controller Access
// References			: NVM Express Specification Revision 1.1a
////////////////////////////////////////////////////////////////////////////////


/* global parameter definitions */
`include "global_parameters"
`define DEBUG_ENABLE

package NvmController;

/* import Bluespec Libraries */
import Vector::*;
import Arbiter::*;
import FIFO::*;
import FIFOF::*;
import SpecialFIFOs::*;
import BRAMFIFO::*;
import BRAM::*;
import DReg::*;
import ConfigReg::*;

/* import user packages */
import global_definitions::*;


/* --- type declarations --- */
/*
The following Structure is defined for use with the "Abort
Command".  The command to be aborted is uniquely identified by the Submission Q
ID (sqID field) and the Command ID (cID field).  Hence this structure is
defined.
*/
typedef struct {
		UInt#(16) sqID;
		UInt#(16) cID;
		} CommandType deriving(Bits,Eq);

/*
The following Structure is used to define the State Machine which "Fetches
Command from the Main Memory".
*/
typedef enum {
	      IDLE,
	      ENQUEUE,
	      FETCHING_COMMAND,
	      WAIT_FOR_COMMAND
	      } Command_Fetch_States deriving (Bits, Eq);

/*
The following Structure is used to define the State Machine which "Executes the
commands".  This state machine starts as soon as the command is ready in the
internalexecQ..
*/
typedef enum {
	      IDLE,
	      EXECUTE_ASQ_COMMAND,
	      ISQ_CHECK_ABORT,
	      EXECUTE_ISQ
	      } Command_Execution_States deriving (Bits, Eq);

/* States declaration for the Completion State Machine. */
typedef enum {
	      IDLE,
	      SEND
	      } Completion_state_type deriving (Bits, Eq);

/* States declaration for the Data Transfer State Machine. */

typedef enum {
	      IDLE,
	      SEND_PRP1,
	      SEND_PRP2,
	      REQUEST_PRP_LIST,
	      GET_PRP_LIST
	      } PRP_transfer_state_type deriving (Bits,Eq);

typedef enum {
	      IDLE,
	      READ_NAND,
	      WRITE_NAND
	      } Data_transfer_state_type deriving (Bits, Eq);

/* States declaration for the Data Structure State Machine. */
typedef enum {
	      IDLE,
	      TRANSMIT
	      } Data_structure_state_type deriving (Bits, Eq);

/* Type declaration for the different data structures. */
typedef enum {
	      IDENTIFY_NAMESPACE,
	      IDENTIFY_CONTROLLER,
	      LBA_RANGE_TYPE
	      } Data_structure_type deriving (Bits, Eq);

/* Info about Data Structures. */
typedef struct {
		Data_structure_type cns;
		UInt#(16) sqid;
		UInt#(16) command_id;
		UInt#(64) prp1_address; //prp1
		UInt#(64) prp2_address; //prp2
		} Data_structure_info_type deriving (Bits, Eq);

/* Info about Data Transfers. */
typedef struct {
		Nand_cmd_opcode nand_cmd_opcode;
		UInt#(16) sqid;
		UInt#(16) command_id;
		UInt#(64) prp1_address; //prp1
		UInt#(64) prp2_address; //prp2
		UInt#(64) logical_block_address;
		UInt#(16) nlb;
		} Data_transfer_info_type deriving (Bits, Eq);

/* Declare the status of a completion. */
typedef enum {
	      SUCCESS,
	      MAX_Q_SIZE_EXCEEDED,
	      INVALID_QID,
	      ABORT_COMMAND_LIMIT_EXCEEDED,
	      ABORTED,
	      FEATURE_NOT_SAVEABLE,
	      UNDEFINED
	      } Completion_status_type deriving (Bits, Eq);

/* Completion Info used by the Completion State Machine. */
typedef struct {
		Bit#(32) dword0;
		Completion_status_type status;
		UInt#(16) sqid;
		UInt#(16) command_id;
		} Completion_info_type deriving (Bits, Eq);


typedef struct {
		Bit#(32) powerManagement;  // Power Management Feature
		Bit#(32) lbaRangeType;  // LBA RANGE Feature *Used as File System*
		Bit#(32) temperatureThreshold;  // temperature threshold limit
		Bit#(32) errorRecovery;  // error recovery Feature
		Bit#(32) volatileWriteCache;  // cache not supported
		Bit#(32) writeAtomicity;  // Write Atomicity
		Bit#(32) asynchronousEventConfig;  // Asyn Event Configuration
		Bit#(32) softwareProgressMarker;  // Software Progress Marker
		Arbitration arbitration; // arbitration
		NumberOfQs_Requested numberR;
		NumberOfQs_Allocated numberA;
		InterruptCoalescing interrupt;
		InterruptVectorConfiguration interruptVectorConfig;
		} Feature_type deriving (Bits, Eq);


interface Ifc_Controller;
   interface Ifc_config ifc_config;
   interface NvmInterruptSideA_Interface nvmInterruptSideA_interface;
   interface Ifc_completion ifc_completion;
   interface NvmTransmitToPCIe_Interface nvmTransmitToPCIe_interface;
   interface Vector#(`NO_CHANNELS, Ifc_nand_flash) ifc_nand_flash;
   interface Ifc_ftl_processor_in ifc_ftl_processor_in;
   interface Vector#(`NO_CHANNELS,Ifc_ftl_processor_out) ifc_ftl_processor_out;
   `ifdef DEBUG_ENABLE
      method Bit#(8) leds_();
   `endif
endinterface

module mkNvmController (Ifc_Controller);

   /* Regs and Wire related to PCIe Completor Requestor Interface */
   Reg#(Bit#(`WDC)) rg_out_data_to_pcie <- mkReg(0);  // Data to be sent to PCIe
   Reg#(UInt#(64)) rg_out_address_to_pcie <- mkReg(0);  // Address being sent to PCIe
   Reg#(Bit#(16)) rg_out_tag_to_pcie <- mkReg(0);  // the tag being requested
   Reg#(Bit#(32)) rg_out_payload_length <- mkReg(0);  // Length of payload in DWords
   Reg#(bit)  rg_out_write_to_pcie <- mkReg(0);  // request to send Write TLP
   Reg#(bit) drg_out_data_valid <- mkDReg(1'b0);  // Indicates that the data being sent is valid
   
   // debug register
   Reg#(Nvm_command_type) rg_debug_nvm_command <- mkRegU();
   
   Wire#(bit) dwr_wait <- mkDWire(0);  // Wait assertion from the PCIe controller

   Reg#(UInt#(TMax#(1, TLog#(TDiv#(`WDC, 64))))) rg_local_prp_list_count <- mkReg(0);

   Reg#(Bit#(`WDC)) rg_local_prp_list_tmp <- mkReg(0);

   Reg#(Bit#(4)) rg_local_cmd_addr <- mkReg(0);  // Address for Command buffer  // TODO TODO SIZE ADJ
   // Command buffer declared as a vector of DWord sized Regs
   Reg#(Vector#(TDiv#(TMul#(64, 8), `WDC), Bit#(`WDC))) rg_command_buf <- mkReg(replicate(0));
   /* if it is reduced to 512 this would half the number of memory */
   Vector#(`NO_CHANNELS, BRAM1Port#(UInt#(TLog#(513)), UInt#(64))) bram_prp_address <-
   replicateM(mkBRAM1Server(defaultValue));
   Vector#(`NO_CHANNELS, FIFO#(UInt#(64))) bff_prp_address <-
   replicateM(mkBypassFIFO());


   // Indicated that the command is ready .. indication to Fetch unit
   Reg#(bit) drg_cmd_compl_received <- mkDReg(0);


   /* Registers for Q management */
   Vector#(`No_SQueues,Reg#(UInt#(16))) sqhdbl <- replicateM(mkReg(0));		// SQ Head Door Bell Registers  // TODO this is not the right name; it is the submission queue head pointer
   Vector#(`No_CQueues,Reg#(UInt#(16))) cqtdbl <- replicateM(mkReg(0));		// CQ Tail Door Bell Registers  // TODO this is not the right name; it is the completion queue tail pointer


   /* I/O Completion Q related ragisters */
   Reg#(Bool) rg_no_outstanding_request <- mkConfigReg(True);
   Vector#(`No_CQueues,Reg#(UInt#(16))) rg_cq_size <- replicateM(mkConfigReg(0));  // Size of Cq to be created
   Vector#(`No_CQueues,Reg#(UInt#(64))) rg_cq_base_address <- replicateM(mkReg(0));
   FIFOF#(Completion_info_type) ff_completion <- mkSizedFIFOF(5);  // TODO 5 entries is only a guess
   Vector#(`NO_CHANNELS, FIFOF#(Completion_info_type)) ff_data_transfer_completion <-
   replicateM(mkSizedFIFOF(5));  // TODO 5 entries is only a guess
   FIFOF#(Completion_info_type) ff_data_structure_completion <- mkSizedFIFOF(5); // TODO 5 entries is only a guess

   Vector#(`No_SQueues,Reg#(UInt#(16))) rg_cqIDofSQ <- replicateM(mkReg(0));  // CQ Identifiers for associated SQs

   /* Interrupt Vector related */
   Vector#(`No_SQueues, Reg#(Bit#(5))) rg_InterruptVector <- replicateM(mkReg(0));  // This Vector holds the Interrupt Vector Numbers for as many as NINE Submission Queues
   Vector#(`No_SQueues, Reg#(bit)) rg_InterruptEnable <- replicateM(mkReg(0));  // Vector to check if Interrupts are enabled to associated SQ

   /* I/O Submission Q related registers */
   Vector#(`No_SQueues,Reg#(bit)) rg_sq_en;
   rg_sq_en[0] <- mkReg(1'b1);  // Admin Submission Queue is always enabled
   for (Integer i = 1; i < `No_SQueues; i = i + 1)
      rg_sq_en[i] <- mkReg(0);
   // Size of Sq to be created
   Vector#(`No_SQueues,Reg#(UInt#(16))) rg_sq_size <- replicateM(mkConfigReg(0));
   Vector#(`No_SQueues,Reg#(UInt#(64))) rg_sq_base_address <- replicateM(mkReg(0));


   /* internal Command Queue */
   FIFOF #(Nvm_command_type) ff_internalexecQ <- mkSizedFIFOF(5);  // Internal FIFO for holding 5 commands

   Reg#(PRP_transfer_state_type) rg_prp_transfer_state <- mkReg(IDLE);
   Vector#(`NO_CHANNELS, Reg#(Data_transfer_state_type)) rg_data_transfer_state <- (
      replicateM(mkReg(IDLE)));
   FIFO#(Data_transfer_info_type) ff_data_transfer_info <- mkSizedFIFO(5);
   // TODO 5 entries is only a guess
   /*
   actually this size should be 1024 * number of logical blocks
   perhaps extend it to 16k?
   */
   FIFO#(Bit#(`WDC)) ff_prp_transfer_data <- mkSizedBRAMFIFO((2*4096*8)/`WDC);
   Vector#(`NO_CHANNELS, FIFO#(Bit#(`WDC))) ff_data_transfer_data <-
   replicateM(mkSizedBRAMFIFO((2 * 4096 * 8) / `WDC));
   //replicateM(mkSizedFIFO((2 * 4096 * 8) / `WDC));  // does not work in hardware; sometimes returns 0 or other strange behaviour


   /* Command related registers */
   Reg#(Nvm_command_type) rg_command <- mkRegU();


   /* Identify Data Structure(DS) related registers */
   // Internal Regs for Reading Data Structures
   Reg#(UInt#(TSub#(15, TLog#(`WDC)))) rg_read_Bram_Count <- mkReg(0);  // BRAM Address count
   Reg#(UInt#(TSub#(15, TLog#(`WDC)))) rg_data_structure_count <- mkReg(0);  // BRAM Address count
   FIFOF#(Data_structure_info_type) ff_data_structure_info <- mkSizedFIFOF(5); // TODO 5 entries is only a guess
   Reg#(Data_structure_state_type) rg_data_structure_state <- mkReg(IDLE);
   Reg#(Data_structure_info_type) rg_local_data_structure_info <- mkRegU();

   Reg#(Completion_state_type) rg_completion_state <- mkReg(IDLE);
   Reg#(Completion_type) rg_local_completion <- mkRegU();
   Reg#(UInt#(16)) rg_local_completion_cqid <- mkReg(0);


   Reg#(Bit#(3)) rg_no_of_outstanding_commands_to_abort <- mkReg(0);  // Indicates the Number of Outstanding Commands left to be aborted

   /* Registers for Command Aborting */
   // List of Commands to be aborted Keerthi:may not be directly useful
   Reg#(Maybe#(CommandType)) abort_command_list[5];
   /*
   A maximum of Five commands can be aborted at a time..v_granted_id
   Valid => command is yet to be aborted
   Invalid => new command can be added to be aborted
   */
   for(Integer i = 0; i < 5; i = i + 1) begin
      abort_command_list[i] <- mkReg(tagged Invalid);
   end

   Reg#(Bool) abort_all <- mkReg(False);  // Abort all
   Reg#(UInt#(16)) rg_cmd_sqid <- mkReg(0);  // This is the sqID of the Command that has to be executed

   /* Controller Registers Definition */
   Controller_capabilities lv_cap = Controller_capabilities {
      reserved1	: 0 ,		// Reserved
      mpsmax	: 4'd0,		// max page size
      mpsmin	: 4'd0,		// Min page size
      reserved2	: 0 ,		// Reserved
      css	: 4'b0001,	//
      reserved3	: 0 ,		// Reserved
      dstrd	: 0,		// DoorBell Stride Value
      to	: 8'd10,	// 10 * 500 ms
      reserved4	: 0 ,		// Reserved
      ams	: 0,		// Only Round Robin supported
      cqr	: 1,		// Physically contiguous memory for SQ CQs
      mqes	: 16'd500		// max Q size = 501
      };

   Reg#(Version) version <- mkReg(Version {
      mjr	: 16'h0001,		// version Major
      mnr	: 16'h0001		// version Minor
      });

   Reg#(Bit#(32)) mask_set <- mkReg(0);		// Interrupt Vectr Mask Set
   Reg#(Bit#(32)) mask_clear <- mkReg(0);	// Interrupt Vectr Mask Clr

   Reg#(Controller_configuration) cc <- mkConfigReg(Controller_configuration {
      reserved1	: 0,				// Reserved
      iocqes	: 0,				// IO Completion Q Entry Size
      iosqes	: 0,				// IO Submission Q Entry Size
      shn	: 0,				// Shutdown Notification
      ams	: 0,				// Arbitration Mechnsm
      mps	: 0,				// Max Page Size
      css	: 0,				// IO Command Set Selected
      reserved2	: 0,				// Reserved
      en	: 0				// Controller Enable
      });

   Reg#(Controller_status) csts <- mkConfigReg(Controller_status {
      reserved	:0,		// Reserved
      shst	:0,		// Shutdwn Status
      cfs	:0,		// Controller Fatal Status
      rdy	:0		// Controller Ready
      });

   Reg#(ASQ) asq <- mkConfigReg(ASQ {
      asqb	:0,		// Admin SQ BAse Adrs
      reserved	:0		// Reserved
      });

   Reg#(ACQ) acq <- mkConfigReg(ACQ {
      acqb	:0,		// Admin CQ Base Address
      reserved	:0		// Reserved
      });

   Vector#(`No_SQueues,Reg#(SQTDBL)) sqtdbl <- replicateM(mkConfigReg(SQTDBL {
      reserved	:0,		// Reserved
      sqt	:0		// SQ TAil DoorBell Value
      }));

   Vector#(`No_SQueues,Reg#(CQHDBL)) cqhdbl <- replicateM(mkConfigReg(CQHDBL {
      reserved	:0,		// Reserved
      cqh	:0		// CQ Head Door Bell Value
      }));
   ////////////////////////////////////////////////////////////////////////////////
   // End of Controller registers
   ////////////////////////////////////////////////////////////////////////////////



   ////////////////////////////////////////////////////////////////////////////////
   // Registers to Store the Features of the Controller
   ////////////////////////////////////////////////////////////////////////////////
   // only round robin is supported
   Reg#(Arbitration) arbitration <- mkReg(Arbitration {
      hpw	: 0,	// high Priority Weight
      mpw	: 0,	// Medium Priority Weight
      lpw	: 0,	// Low Priority Weight
      rsv	: 0,	// reserved bit
      ab	: 0	// Arbitration Burst
      });

   /* Meaning of index:
   0: current feature value
   1: default feature value
   2: saved feature value
   */
   Vector#(3, Reg#(Feature_type)) rg_feature <- (
      replicateM(mkReg(Feature_type {
	 powerManagement		: 0,	// Power Management Feature
	 lbaRangeType			: 0,	// LBA RANGE Feature *Used as File System*
	 temperatureThreshold		: 120,	// temperature threshold limit
	 errorRecovery			: 0,	// error recovery Feature
	 volatileWriteCache		: 0,	// cache not supported
	 writeAtomicity			: 0,	// Write Atomicity
	 asynchronousEventConfig	: 0,	// Asyn Event Configuration
	 softwareProgressMarker		: 0,	// Software Progress Marker
	 interruptVectorConfig		: InterruptVectorConfiguration {
	    cd	: 0,				// Coalesceing Disable
	    iv	: 0				// Interupt Vector
	    },
	 interrupt			: InterruptCoalescing {
	    int_time	: 0,			// Int Aggregation time
	    thr		: 0			// Int Aggregation Threshold
	    },
	 numberR			: NumberOfQs_Requested {
	    ncqr	: 0,			// Number of IO CQs Requested
	    nsqr	: 0			// Number of IO SQs Requested
	    },
	 numberA			: NumberOfQs_Allocated {
	    ncqa	: `No_CQueues,		// Number of IO CQs Allocated
	    nsqa	: `No_SQueues		// Number of IO SQs Allocated
	    },
	 arbitration			: (Arbitration {
	    hpw	: 0,				// high Priority Weight
	    mpw	: 0,				// Medium Priority Weight
	    lpw	: 0,				// Low Priority Weight
	    rsv	: 0,				// reserved bit
	    ab	: 0				// Arbitration Burst
	    })					// arbitration
	 })));


   ////////////////////////////////////////////////////////////////////////////////
   // Registers for Facilitating MSI interrupts
   ////////////////////////////////////////////////////////////////////////////////
   Reg#(Bit#(32)) mask_reg <- mkConfigReg(0);  // Internal Register for Interrupt Mask
   Reg#(Bit#(32)) status_reg <- mkReg(0);  // Interrupt Status register
   
   Wire#(Bit#(32)) dwr_clear_mask <- mkDWire(0);
   Wire#(Bit#(32)) dwr_set_mask <- mkDWire(0);
   
   Wire#(Bit#(32)) dwr_compl_int <- mkDWire(0);
   
   Reg#(Bit#(5)) vectr <- mkReg(0);
   Reg#(Bit#(1)) vectr_rdy <- mkReg(0);
   
   Reg#(Bit#(5)) vectr_count[31];
   for(Integer i = 0; i < 31; i = i + 1) begin
      vectr_count[i] <- mkReg(0);
   end

   Reg#(Bit#(5)) aggr_threshold <- mkReg(0);   // TODO TODO TODO 3 make adjustable

   /* Counter which can be used by the software via Configuration Interface */
   Reg#(Bit#(64)) rg_ns_count <- mkReg(0);  // clk frequency of 100 MHz is assumed
   Reg#(Bool) rg_count_started <- mkReg(False);


   /* Module Instantiations */
   // fixed = True i.e. it gives the current client the priority  Keerthi:may be
   Arbiter_IFC#(`No_SQueues) rr_arb <- mkArbiter(False);
   
   /* PCIe Arbiter
   
   A sticky arbiter is used, thus the clients can keep the grant during their
   transmission

   The indices are:
   0 - Completion State Machine
   1 - ASQ_READ_DATA_STRUCTURE
   2 - Fetch State Machine
   3 - Data Transfer State Machine - Channel 0
   4 - Data Transfer State Machine - Channel 0
   .
   .
   */
   Arbiter_IFC#(TAdd#(`NO_CHANNELS, 4)) rr_pcie <- mkStickyArbiter();

   /*
   Please refer to NVM Express Specification Revision 1.1a (September 23, 2013)
   section 4.6 for information about the use of phase tags.
   */
   Vector#(TAdd#(`No_CQueues, 1), Reg#(bit)) rg_phase_tags <-
   replicateM(mkReg(1'b1));
   

   ////////////////////////////////////////////////////////////////////////////////
   // BRAMs for Identify Controller Data Structure and Identify Namespace Data Structure
   ////////////////////////////////////////////////////////////////////////////////
   /*
   Buffer for Controller Data Structure
   The contents are initialized by using the text file named
   "IdentifyControllerDataStructure.txt".
   */
   BRAM_Configure cfg_controller_ds = defaultValue;
   cfg_controller_ds.loadFormat = tagged Hex ("IdentifyControllerDataStructure_" + `WDC_STRING + ".txt");
   
   BRAM1Port#(Bit#(TSub#(15, TLog#(`WDC))), Bit#(`WDC)) controller_data_structure <-
   mkBRAM1Server(cfg_controller_ds);
   
   
   /*
   Buffer for NameSpace Data Structure
   The contents are initialized by using the text file named
   "IdentifyNamespaceDataStructure.txt".
   */
   BRAM_Configure cfg_namespace_ds = defaultValue;
   cfg_namespace_ds.loadFormat = tagged Hex ("IdentifyNamespaceDataStructure_" + `WDC_STRING + ".txt");

   BRAM1Port#(Bit#(TSub#(15, TLog#(`WDC))) , Bit#(`WDC)) namespace_data_structure <-
   mkBRAM1Server(cfg_namespace_ds);


   ////////////////////////////////////////////////////////////////////////////////
   // BRAM for LBA Range Data Structures
   ////////////////////////////////////////////////////////////////////////////////
   /*
   Buffer for LBA Range Data Structure
   The contents are initialized by using the text file named
   "LBARangeDataStructure.txt".
   */
   BRAM_Configure cfg_lbaRange_ds = defaultValue;
   cfg_lbaRange_ds.loadFormat = tagged Hex ("LBARangeDataStructure_" + `WDC_STRING + ".txt");

   BRAM1Port#(Bit#(TSub#(15, TLog#(`WDC))) , Bit#(`WDC)) lbaRange_data_structure <-
   mkBRAM1Server(cfg_lbaRange_ds);


   /* BRAM for Write Data Buffer */
   BRAM_Configure cfg_write_data_buffer = defaultValue;
   BRAM1Port#(Bit#(TSub#(15, TLog#(`WDC))) , Bit#(`WDC)) write_data_buffer <-
   mkBRAM1Server(cfg_write_data_buffer);


   `ifdef DEBUG_ENABLE
   Reg#(Bit#(8)) rg_out_leds <- mkReg('h33);  // Test LEDs
   Reg#(Bit#(8)) rg_out_leds1 <- mkReg('h33);  // Test LEDs
   Reg#(Bit#(8)) rg_out_leds2 <- mkReg('h33);  // Test LEDs
   `endif

   /*
   Q Empty Condition --> Tail = Head
   Q Full Condition  --> Tail = Head - 1
   One location is always vacant
   */
   function Bool fn_sq_empty(UInt#(16) sqid);
   /* Determine if the submission queue with the id 'sqid' is full.

   Full for a queue means that head and tail pointer hold the same vaule.
   */
   return (sqhdbl[sqid] == sqtdbl[sqid].sqt);
   endfunction

   function UInt#(TLog#(TAdd#(`NO_CHANNELS, 4))) fn_rr_pcie_id();
      /* Get the id of the client which got the grant.
   
      One extra value (the highest) is added to express that there is no grant.
      */
      // default to no grant (`NO_CHANNELS + 4 is higher than all used values)
      UInt#(TLog#(TAdd#(`NO_CHANNELS, 4))) lv_granted_id =
      fromInteger(valueOf(TAdd#(`NO_CHANNELS, 4)));
      for (Integer i = 0; i < `NO_CHANNELS + 4; i = i + 1) begin
	 if (rr_pcie.clients[i].grant)
	    lv_granted_id = fromInteger(i);
      end
      return lv_granted_id;
   endfunction

   function Completion_type fn_build_completion(Completion_info_type
						completion_info);
      /* Construct the completion structure with the given information. */
      let lv_cqID = rg_cqIDofSQ[completion_info.sqid];
      let lv_CQ_DWord2 = CQ_DWord2 {
	 sqID: completion_info.sqid,
	 sqHeadPointer: sqhdbl[completion_info.sqid]
	 };
   
      let lv_CQ_DWord3 = CQ_DWord3 {
	 status_field_DNR : 0,
	 status_field_M   : 0 ,
	 status_field_res : 0 ,
	 status_field_SCT : 0 ,
	 status_field_SC  : 0 ,
	 phase_tag        : 0,
	 commandID	  : 0
      };
      // set DWord3 according to the completion status
      case (completion_info.status)
      // TODO TODO add abort status
      SUCCESS:
      lv_CQ_DWord3 = CQ_DWord3 {
	 status_field_DNR	: 0 , // NA
	 status_field_M		: 0 , // No more Error log info
	 status_field_res	: 0 , // Reserved
	 status_field_SCT	: 0 , // Generic Command Status
	 status_field_SC	: 0 , // Successful Completion
	 phase_tag		: rg_phase_tags[lv_cqID],
	 commandID		: completion_info.command_id
	 };
      MAX_Q_SIZE_EXCEEDED:
      lv_CQ_DWord3 = CQ_DWord3 {
	 status_field_DNR	: 1 , // Do Not Retry With The Same Command
	 status_field_M		: 0 , // No more Error log info
	 status_field_res	: 0 , // Reserved
	 status_field_SCT	: 1 , // Command Specific Status
	 status_field_SC	: 2 , // Maximum Queue Size Exceeded
	 phase_tag		: rg_phase_tags[lv_cqID],
	 commandID		: completion_info.command_id
	 };
      INVALID_QID:
      lv_CQ_DWord3 = CQ_DWord3 {
	 status_field_DNR	: 1 , // Do Not Retry With The Same Command
	 status_field_M		: 0 , // No more Error log info
	 status_field_res	: 0 , // Reserved
	 status_field_SCT	: 1 , // Command Specific Status
	 status_field_SC	: 1 , // Invalid Queue Identifier
	 phase_tag		: rg_phase_tags[lv_cqID],
	 commandID		: completion_info.command_id
	 };
      ABORT_COMMAND_LIMIT_EXCEEDED:
      lv_CQ_DWord3 = CQ_DWord3 {
	 status_field_DNR	: 1 , // Do Not Retry With The Same Command
	 status_field_M		: 0 , // No more Error log info
	 status_field_res	: 0 , // Reserved
	 status_field_SCT	: 1 , // Command Specific Status
	 status_field_SC	: 3 , // Abort Command Limit Exceeded
	 phase_tag		: rg_phase_tags[lv_cqID],
	 commandID		: completion_info.command_id
	 };
      FEATURE_NOT_SAVEABLE:
      lv_CQ_DWord3 = CQ_DWord3 {
	 status_field_DNR	: 1, // Do Not Retry With The Same Command
	 status_field_M		: 0, // No more Error log info
	 status_field_res	: 0, // Reserved
	 status_field_SCT	: 1, // Command Specific Status
	 status_field_SC	: 'h0d, // Abort Command Limit Exceeded
	 phase_tag		: rg_phase_tags[lv_cqID],
	 commandID		: completion_info.command_id
	 };
   endcase

   return Completion_type {
      dword0	: completion_info.dword0,
      dword1	: 32'h0,  // CQ_DWORD1 is reserved
      dword2	: lv_CQ_DWord2,
      dword3	: lv_CQ_DWord3
      };
   endfunction

   rule rl_Initialization;
      /* Rule for Controller Initialization
      
      When the Host sets the cc.en bit to 1.. Controller responds to it by setting
      the controler status to ready
      */
      if(cc.en == 1) begin
	 csts.rdy <= 1;
      end else csts.rdy <= 0;
   endrule
   
   
   rule rl_set_admin_values;
      // always enable interrupts in admin queue
      rg_InterruptEnable[0] <= 1'b1;
      // zero is interrupt vector for admin queue
      rg_InterruptVector[0] <= 0;
      // set the admin submission queue base address
      rg_sq_base_address[0] <= unpack({asq.asqb , asq.reserved});
      // set admin completion queue base address
      rg_cq_base_address[0] <= unpack({acq.acqb,12'd0});
   endrule: rl_set_admin_values
   
   // The following registers are used for implementing Command Fetch State Machine
   Reg#(Command_Fetch_States) rg_fetch_state <- mkReg(IDLE);
   Reg#(UInt#(TLog#(`No_SQueues))) rg_local_last_grant <- mkReg(1);  // TODO TODO initial value
   FIFOF#(UInt#(16)) sqID_fifo <- mkSizedFIFOF(5);

   /* Begin Rules for Requesting the Arbiter
   
   If the SQ is eligible for requesting then the following Rules are used to
   Request the Arbiter for granting the Access
   
   Keeps requesting the arbiter as long as it is eligible to request
   
   If the particular Q is not empty then that Q is Eligible for Request
   Ofcourse The Q has to be Created first
   Admin Q need not be created
   NVM IO SQ has to be created and then checked for empty or not
   */
   for (Integer r = 0; r < `No_SQueues; r = r + 1) begin
      rule  rl_sq_req ((!fn_sq_empty(fromInteger(r))) && (rg_sq_en[r] == 1) &&
		       (cc.en == 1) && rg_fetch_state == IDLE);
	 rr_arb.clients[r].request;
      endrule : rl_sq_req
   end
   /* End Rules for Requesting the Arbiter */

   /* Begin of Command Fetch state machine. */

   /*
   The config interface has priority when writing to the FIFO.  This may result in
   a pause of execution if the config interface continuously writes.
   
   (* descending_urgency = "ifc_config__write, rl_fetch_idle_state" *)
   (* descending_urgency = "ifc_config__write, rl_fetch_wait_for_command_state" *)
   */
   
   rule rl_fetch_idle_state (rg_fetch_state == IDLE && cc.en == 1);
      /* Wait for grant from round robin arbiter and start fetching the command. */
      if (rr_arb.grant_id() != 0 || (rr_arb.grant_id() == 0 &&
	 rr_arb.clients[0].grant()))
	 begin
	    /* there is a new grant for a submission queue */
	    $display("%d: ********** SQ %d got the grant ********", $stime(),
	       rr_arb.grant_id());
	    rg_cmd_sqid <= extend(unpack(rr_arb.grant_id()));
	    rg_fetch_state <= FETCHING_COMMAND;
	 end
   endrule: rl_fetch_idle_state


   (* fire_when_enabled, no_implicit_conditions *)
   rule rl_fetching_command0 (rg_fetch_state == FETCHING_COMMAND &&
			      ff_internalexecQ.notFull());
      /* Request the PCIe interface (only when the internal buffer is free)
      until granted and hold the request and thus
      grant (sticky) until data is sent.
      */
      rr_pcie.clients[2].request();
   endrule: rl_fetching_command0


   (* fire_when_enabled, no_implicit_conditions *)
   rule rl_fetching_command1 (rg_fetch_state == FETCHING_COMMAND &&
			      (fn_rr_pcie_id == 2 && dwr_wait == 0));
      /* Send the request to get the command over PCIe.

      This rule only fires if PCIe Interface access is granted.
      */
      $display("%d: # STATE INFO # COMMAND FETCH STATE # FETCHING COMMAND ",
	       $stime());
      /* PCIe Interface is ready to receive */
      rg_out_write_to_pcie <= 0;
      rg_out_tag_to_pcie <= 'd1;
      rg_out_payload_length <= 'd16;
      // request data from the position indicated by the sqhdbl pointer
      $display("%d: %d,%d, %d", $stime(), rg_cmd_sqid,
	       rg_sq_base_address[rg_cmd_sqid], sqhdbl[rg_cmd_sqid]);
      rg_out_address_to_pcie <= rg_sq_base_address[rg_cmd_sqid] +
      extend(sqhdbl[rg_cmd_sqid]) * `SQ_ENTRY_SIZE;
      drg_out_data_valid <= 1;
      rg_fetch_state <= WAIT_FOR_COMMAND;
   endrule: rl_fetching_command1


   rule rl_fetch_wait_for_command_state (rg_fetch_state == WAIT_FOR_COMMAND);
      /* Wait for the command to be received by the Completion Interface and
      enqueue it into the Execution FIFO. */
      $display("%d: # STATE INFO # COMMAND FETCH STATE # WAIT FOR COMMAND ",
	 $stime());
      if(drg_cmd_compl_received == 1)
	 begin
	    $display ("%d: NvmController: enqueuing command into execution queue\n");
	    /* completion received by ifc_completion._write */
	    // enqueue the command for Execution state machine
	    
	    rg_debug_nvm_command <= unpack(pack(rg_command_buf));
	    
	    ff_internalexecQ.enq(unpack(pack(rg_command_buf)));
	    // enqueue the corresponding sqid
	    sqID_fifo.enq(rg_cmd_sqid);
	    
	    // update the submission queue head doorbell
	    UInt#(16) lv_sqhdbl;
	    if(sqhdbl[rg_cmd_sqid] == rg_sq_size[rg_cmd_sqid])
	       lv_sqhdbl = 0;
	    else
	       lv_sqhdbl = sqhdbl[rg_cmd_sqid] + 1;
	    sqhdbl[rg_cmd_sqid] <= lv_sqhdbl;
	    
	    rg_fetch_state <= IDLE;
	 end
   endrule: rl_fetch_wait_for_command_state
   /* End of Command Fetch state machine. */
   
   Reg#(Command_Execution_States) rg_execution_state <- mkReg(IDLE);
   Reg#(UInt#(16)) command_sqID <- mkReg(0);

   rule rl_IDLE (rg_execution_state == IDLE);
      /* Get the next command from the Execution FIFO and start execution. */
      $display("%d: # STATE INFO # COMMAND EXEC STATES # EXECUTION IDLE", $stime());
      // get the next command from execution fifo
      rg_command <= ff_internalexecQ.first();
      if (sqID_fifo.first() == 0) begin
	 $display("%d:  THE OPCODE FOR THE ASQ COMMAND IS = %d ", $stime(),
		  ff_internalexecQ.first().opcode);	
	 `ifdef DEBUG_ENABLE
	 rg_out_leds1 <= 1;
	 `endif
	 command_sqID <= 0;
	 rg_execution_state <= EXECUTE_ASQ_COMMAND;
      end else
	     begin
		$display("%d:  THE OPCODE FOR THE IO COMMAND IS = %d ", $stime(),
			 ff_internalexecQ.first().opcode);
		
		`ifdef DEBUG_ENABLE
		rg_out_leds1 <= 2;
		`endif
		command_sqID <= sqID_fifo.first();
		rg_execution_state <= ISQ_CHECK_ABORT;
	     end
      ff_internalexecQ.deq();
      sqID_fifo.deq();
   endrule


   rule rl_asq_command_execution (rg_execution_state == EXECUTE_ASQ_COMMAND);
      /* Execute the Admin Command. */
      $display("%d: # STATE INFO # COMMAND EXEC STATES # ASQ EXECUTION IN PROGRESS",
	       $stime());
      `ifdef DEBUG_ENABLE
      rg_out_leds1 <= 3;
      `endif
      case (rg_command.opcode)

	 8'h00: begin
		   /* Opcode : 00h		Delete I/O Submission Q Command */
		   $display("%d:  Delete I/O Submission Q ", $stime());
		   // the QID for the associated SQ to be Deleted
		   let qID = rg_command.cdw10[15:0];
		   Completion_status_type lv_completion_status = UNDEFINED;
		   if (qID < rg_feature[0].numberA.nsqa && qID != 0) begin
		      rg_sq_en[qID] <= 0;			// Disable the Q
		      lv_completion_status = SUCCESS;
		   end else begin
			       lv_completion_status = INVALID_QID;
			    end
		   ff_completion.enq(Completion_info_type {
		      dword0		: 0,
		      status		: lv_completion_status,
		      sqid		: command_sqID,
		      command_id	: rg_command.command_id
		      });
		   rg_execution_state <= IDLE;
		end

	 8'h01: begin
		   /* Opcode : 01h		Create I/O Submission Q Command */
		   $display("%d:  Create I/O Submission Q ", $stime());
		   Completion_status_type lv_completion_status = UNDEFINED;
		   if (rg_command.fuse == 2'b00) begin		// Indicates Normal Operation
		      let qID = rg_command.cdw10[15:0];		// The QID for the asociated SQ to be created
		      if(qID < rg_feature[0].numberA.nsqa && qID != 0) begin
			 if (rg_command.cdw10[31:16] < lv_cap.mqes) begin
			    // TODO check if queue already in use
			    //$display("%d:  SQ %d is enabled .. it can contain commands now ", $stime(),  qID); // SQ 2 is ISQ 1 ... SQ0 is ASQ
			    //$display("%d:  @SIZE OF THE CREATED QUEUE = %d @", $stime(), rg_command.cdw10[31:16]);
			    rg_sq_en[qID] <= 1;  // Enabling the Q with particular qID. i.e Creating it
			    rg_sq_size[qID] <= unpack(rg_command.cdw10[31:16]);  // Size of the Q
			    rg_sq_base_address[qID] <= rg_command.prp1;  // NEED TO SEE MORE CDW11.PC ***** CAREFUL *****
			    lv_completion_status = SUCCESS;  // Command Executed Successfuly
			    rg_cqIDofSQ[qID] <= unpack(rg_command.cdw11[31:16]);  // Identifier of CQ to utilize for completons for this SQID..
			 end
			 else
			    begin
			       lv_completion_status = MAX_Q_SIZE_EXCEEDED;  // Command Execution FAILED
			    end
		      end
		      else
			 begin
			    lv_completion_status = INVALID_QID;  // Command Execution FAILED
			 end
		   end // end of fuse if
		   ff_completion.enq(Completion_info_type {
		      dword0		: 0,
		      status		: lv_completion_status,
		      sqid		: command_sqID,
		      command_id	: rg_command.command_id
		      });
		   rg_execution_state <= IDLE;
		end

	 8'h04: begin
		   /* Opcode : 04h		Delete I/O Completion Q Command */
		   $display("%d:  Delete I/O Completion Q ", $stime());
		   // The QID for the asociated CQ to be Deleted
		   let qID = rg_command.cdw10[15:0];
		   Completion_status_type lv_completion_status = UNDEFINED;
		   if (qID < rg_feature[0].numberA.ncqa) begin
		      // TODO TODO do something
		      lv_completion_status = SUCCESS;
		   end
		   else 
		      begin
			 lv_completion_status = INVALID_QID;
		      end
		   ff_completion.enq(Completion_info_type {
		      dword0		: 0,
		      status		: lv_completion_status,
		      sqid		: command_sqID,
		      command_id	: rg_command.command_id
		      });
		   rg_execution_state <= IDLE;
		end

	 8'h05: begin
		   /* Opcode : 05h		Create I/O Completion Q Command */
		   $display("%d:  Create I/O Completion Queue, id: %d, base address: 0x%x",
		      $stime(), rg_command.cdw10[15:0], rg_command.prp1);
		   Completion_status_type lv_completion_status = UNDEFINED;
		   if (rg_command.fuse == 2'b00) begin  // Indicates Normal Operation
		      // The QID for the asociated CQ to be created
		      let qID = rg_command.cdw10[15:0];
		      if(qID < rg_feature[0].numberA.ncqa && qID != 0) begin
			 if (rg_command.cdw10[31:16] < lv_cap.mqes) begin
			    $display("%d: completion queue is really created", $stime());
			    rg_cq_size[qID] <= unpack(rg_command.cdw10[31:16]);
			    rg_cq_base_address[qID] <= rg_command.prp1;  // NEED TO SEE MORE CDW11.PC ***** CAREFUL *****
			    lv_completion_status = SUCCESS;

			    // Interrupts for the CQs
			    /*
			    Using only 5 bits out of the 16 bits specified. bcoz SP6
			    endpoint core suports only 32 vectors
			    */
			    rg_InterruptVector[qID] <= rg_command.cdw11[20:16];
			    rg_InterruptEnable[qID] <= rg_command.cdw11[1];

			 end
			 else
			    begin
			       $display("%d: create completion queue: MAX_Q_SIZE_EXCEEDED %d", $stime(), rg_command.cdw10[31:16]);
			       lv_completion_status = MAX_Q_SIZE_EXCEEDED;
			    end
		      end
		      else
			 begin
			    $display("%d: create completion queue: INVALID_QID", $stime());
			    lv_completion_status = INVALID_QID;
			 end

		   end
		   else
		      begin
			 $display("FUSE IS NOT SUPPORTED.");
		      end
		   ff_completion.enq(Completion_info_type {
		      dword0		: 0,
		      status		: lv_completion_status,
		      sqid		: command_sqID,
		      command_id	: rg_command.command_id
		      });
		   rg_execution_state <= IDLE;
		end

	 8'h06: begin
		   /* Opcode : 06h - Identify Command */
		   $display("%d:  Identify Command %d", $stime(), rg_command.cdw10[1:0]);
		   case (rg_command.cdw10[1:0])
		      'b00:
		      ff_data_structure_info.enq(Data_structure_info_type {
			 cns		: IDENTIFY_NAMESPACE,
			 sqid		: command_sqID,
			 command_id	: rg_command.command_id,
			 prp1_address	: rg_command.prp1,
			 prp2_address	: rg_command.prp2
			 });
		      'b01:
		      ff_data_structure_info.enq(Data_structure_info_type {
			 cns		: IDENTIFY_CONTROLLER,
			 sqid		: command_sqID,
			 command_id	: rg_command.command_id,
			 prp1_address	: rg_command.prp1,
			 prp2_address	: rg_command.prp2
			 });
		      'b10:
		      // TODO TODO do something
		      ff_completion.enq(Completion_info_type {
			 dword0		: 0,
			 status		: SUCCESS,
			 sqid		: rg_local_data_structure_info.sqid,
			 command_id	: rg_local_data_structure_info.command_id
			 });
		      /*
		      'b11:
		      // TODO TODO reserved; error handling
		      */
		   endcase
		   rg_execution_state <= IDLE;
		end
	 
	 8'h08: begin
		   /* Opcode : 08h - Abort Command */
		   $display("%d:  ABORT Command ", $stime());
		   
		   /*
		   The Value of  5 is the max value of oustanding abort commands supported
		   This value is specified in the identify controller data structure.
		   page 55 NVMe Spec
		   */
		   Completion_status_type lv_completion_status = UNDEFINED;
		   if (rg_no_of_outstanding_commands_to_abort == 'd5) begin
		      lv_completion_status = ABORT_COMMAND_LIMIT_EXCEEDED;
		   end
		   else
		      begin
			 // get next free index in abort command list
			 UInt#(5) lv_id = 0;  // TODO parametrize
			 for (Integer i = 4; i >= 0; i = i - 1) begin  // TODO parametrize
			    if (!isValid(abort_command_list[i]))
			       lv_id = fromInteger(i);
			 end
			 // put the command in the abort list
			 abort_command_list[lv_id] <= tagged Valid CommandType {
			    sqID : unpack(rg_command.cdw10[15:0]),
			    cID  : unpack(rg_command.cdw10[31:16])
			    };
			 rg_no_of_outstanding_commands_to_abort <=
			 rg_no_of_outstanding_commands_to_abort + 1;
			 lv_completion_status = SUCCESS;
		      end
		   ff_completion.enq(Completion_info_type {
		      dword0		: 0,
		      status		: lv_completion_status,
		      sqid		: command_sqID,
		      command_id	: rg_command.command_id
		      });
		   rg_execution_state <= IDLE;
		end

	 8'h09: begin  // SET Features
		   /* Opcode : 09h - SET FEATURES Command */
		   $display("%d:  SET FEATURES command ", $stime());
		   let featureID = rg_command.cdw10[7:0];
		   let lv_save = rg_command.cdw10[31];
		   if (lv_save == 1'b1) begin
		      ff_completion.enq(Completion_info_type {
			 dword0		: 0,
			 status		: FEATURE_NOT_SAVEABLE,
			 sqid		: command_sqID,
			 command_id	: rg_command.command_id
			 });
		   end
		   else
		      begin
			 if(featureID == 8'h03) begin  // TODO TODO no completion here ???
			    rg_feature[0].lbaRangeType[5:0] <= rg_command.cdw11[5:0];
			    /*
			    The data buffer has to be obtained from the address specified in the
			    PRP field
			    */
			 end
			 else
			    begin
			       case (featureID)
				  8'h01:
				  rg_feature[0].arbitration <=  Arbitration {
					hpw	: rg_command.cdw11[31:24],
					mpw	: rg_command.cdw11[23:16],
					lpw	: rg_command.cdw11[15:8],
					rsv	: 0,  // reserved bit
					ab	: rg_command.cdw11[2:0]
					};
				  8'h02:
				  rg_feature[0].powerManagement[4:0] <= rg_command.cdw11[4:0];
				  8'h04:
				  rg_feature[0].temperatureThreshold[15:0] <= rg_command.cdw11[15:0];
				  8'h05:
				  rg_feature[0].errorRecovery[15:0] <= rg_command.cdw11[15:0];
				  8'h06:
				  rg_feature[0].volatileWriteCache[0] <= rg_command.cdw11[0];
				  8'h07:
				  rg_feature[0].numberR <= NumberOfQs_Requested {
				     ncqr	: rg_command.cdw11[31:16],
				     nsqr	: rg_command.cdw11[15:0]
				     };
				  8'h08:
				  rg_feature[0].interrupt <= InterruptCoalescing {
				     int_time	: rg_command.cdw11[15:8],
				     thr	: rg_command.cdw11[7:0]
				     };
				  8'h09:
				  rg_feature[0].interruptVectorConfig <= InterruptVectorConfiguration {
				     cd		: rg_command.cdw11[16],
				     iv		: rg_command.cdw11[15:0]
				     };
				  8'h0a:
				  rg_feature[0].writeAtomicity[0] <= rg_command.cdw11[0];
				  8'h0b:
				  rg_feature[0].asynchronousEventConfig[15:0] <= rg_command.cdw11[15:0];
				  8'h80:
				  rg_feature[0].softwareProgressMarker[7:0] <= rg_command.cdw11[7:0];
			       endcase

			       ff_completion.enq(Completion_info_type {
			         dword0		: 0,
			         status		: SUCCESS,
			         sqid		: command_sqID,
			         command_id	: rg_command.command_id
			       });
			       rg_execution_state <= IDLE;
			    end
		      end
		end

	 8'h0a: begin  // Get Features Command
		   /* Opcode : 0Ah - GET FEATURES Command */
		   $display("%d:  GET FETURES Command ", $stime());
		   let featureID = rg_command.cdw10[7:0];
		   let lv_sel = rg_command.cdw10[10:08];
		   if (lv_sel == 3) begin
		      ff_completion.enq(Completion_info_type {
			 dword0		: 0,
			 status		: SUCCESS,
			 sqid		: command_sqID,
			 command_id	: rg_command.command_id
			 });
		   end
		   else
		      begin
			 if(featureID == 8'h03) begin
			    // TODO return current, default etc..
			    ff_data_structure_info.enq(Data_structure_info_type {
			    cns		: LBA_RANGE_TYPE,
			    sqid		: command_sqID,
			    command_id	: rg_command.command_id,
			    prp1_address	: rg_command.prp1,
			    prp2_address	: rg_command.prp2
			    });
			    rg_execution_state <= IDLE;
			 end
			 else
			    begin
			       let lv_dword0 = 0;
			       case (featureID)
				  8'h01:
				  lv_dword0 = {
				     rg_feature[lv_sel].arbitration.hpw,
				     rg_feature[lv_sel].arbitration.mpw,
				     rg_feature[lv_sel].arbitration.lpw,
				     rg_feature[lv_sel].arbitration.rsv,  // reserved bit
				     rg_feature[lv_sel].arbitration.ab
				     };
				  8'h02:
				  lv_dword0 = rg_feature[lv_sel].powerManagement;
				  8'h04:
				  lv_dword0 = rg_feature[lv_sel].temperatureThreshold;
				  8'h05:
				  lv_dword0 = rg_feature[lv_sel].errorRecovery;
				  8'h06:
				  lv_dword0 = rg_feature[lv_sel].volatileWriteCache;
				  8'h07:
				  lv_dword0 = {
				     rg_feature[lv_sel].numberA.ncqa,
				     rg_feature[lv_sel].numberA.nsqa
				     };
				  8'h08:
				  lv_dword0 = {
				     16'd0,
				     rg_feature[lv_sel].interrupt.int_time,
				     rg_feature[lv_sel].interrupt.thr
				     };
				  8'h09:
				  lv_dword0 = {
				     15'd0,
				     rg_feature[lv_sel].interruptVectorConfig.cd,
				     rg_feature[lv_sel].interruptVectorConfig.iv
				     };
				  8'h0a:
				  lv_dword0 = rg_feature[lv_sel].writeAtomicity;
				  8'h0b:
				  lv_dword0 = rg_feature[lv_sel].asynchronousEventConfig;
				  8'h80:
				  lv_dword0 = rg_feature[lv_sel].softwareProgressMarker;
			       endcase
			       ff_completion.enq(Completion_info_type {
			       dword0		: lv_dword0,
			       status		: SUCCESS,
			       sqid		: command_sqID,
			       command_id	: rg_command.command_id
			       });
			       rg_execution_state <= IDLE;
			    end
		      end
		end	 
      endcase
   endrule

   rule rl_isq_check_abort (rg_execution_state == ISQ_CHECK_ABORT);  // TODO TODO what happens if commands are already executed -> list becomes full
      /* Check if the command to be executed has to be aborted. */
      $display("%d: STATE INFO: CHECKING ISQ ABORT #", $stime());
      `ifdef DEBUG_ENABLE
      rg_out_leds1 <= 4;
      `endif
      Bool lv_aborted = False;
      
      for (Integer i = 0; i < 5; i = i + 1) begin
	 if(abort_command_list[i] matches tagged Valid .commandType &&&
	    (commandType.sqID == command_sqID  &&
	     commandType.cID == rg_command.command_id))
	    begin
	       lv_aborted = True;
	       abort_command_list[i] <= tagged Invalid;
	    end
      end
      
      if (lv_aborted == False) begin
	 /* no abortion -> start execution */
	 $display("%d: # COMMAND NOT ABORTED #", $stime());
	 rg_execution_state <= EXECUTE_ISQ;
      end
      else
	 begin
	    /* abort command */
	    $display("%d: Command Aborted", $stime());
	    rg_no_of_outstanding_commands_to_abort <=
	    rg_no_of_outstanding_commands_to_abort - 1;
	    ff_completion.enq(Completion_info_type {
	       dword0: 0,
	       status: ABORTED,
	       sqid: command_sqID,
	       command_id: rg_command.command_id
	       });
	    rg_execution_state <= IDLE;
	 end
   endrule
   
   // Registers for NVM to NAND Interface Signals
   Reg#(UInt#(64)) rg_flush_data_from <- mkReg(0);
   Reg#(Bit#(64)) rg_logical_block_address <- mkReg(0);

   rule rl_isq_execution (rg_execution_state == EXECUTE_ISQ);
      /* Execute the IO Command. */
      $display("%d:  ******* NVM Command Execution, opcode %d", $stime(),
	       rg_command.opcode);

      case (rg_command.opcode)
	 'h00: begin // Flush
		  `ifdef DEBUG_ENABLE
		  rg_out_leds1 <= 6;
		  `endif
		  rg_flush_data_from <= rg_command.prp1;
		  rg_logical_block_address <= {rg_command.cdw11, rg_command.cdw10};
		  //	rg_initiate_flush <= True;
	       end
	 'h01: begin  // Write
		  `ifdef DEBUG_ENABLE
		  rg_out_leds1 <= 7;
		  `endif
		  $display("%d: * NVM EXPRESS * WRITE COMMAND *", $stime());
		  $display("%d: * Data is to be taken from location = 0x%x", $stime(),
			   rg_command.prp1);
		  /* enqueue the data write request into the Data Transfer FIFO */
		  ff_data_transfer_info.enq(Data_transfer_info_type {
		     nand_cmd_opcode		: WRITE_NAND,
		     sqid			: command_sqID,
		  command_id		: rg_command.command_id,
		  prp1_address		: rg_command.prp1,
		  prp2_address		: rg_command.prp2,
		  nlb			: unpack(rg_command.cdw12[15:0]),
		  logical_block_address	: unpack({rg_command.cdw11, rg_command.cdw10})
		  });
		  rg_execution_state <= IDLE;
	       end
	 'h02: begin // Read
		  `ifdef DEBUG_ENABLE
		  rg_out_leds1 <= 8;
		  `endif
		  $display("%d: * NVM EXPRESS * READ COMMAND *", $stime());
		  $display("%d: Transfer the Read Data to memory location %x", $stime(),
			   rg_command.prp1);
		  /* enqueue the data read request into the Data Transfer FIFO */
		  ff_data_transfer_info.enq(Data_transfer_info_type {
		     nand_cmd_opcode		: READ_NAND,
		     sqid			: command_sqID,
		     command_id			: rg_command.command_id,
		     prp1_address		: rg_command.prp1,
		     prp2_address		: rg_command.prp2,
		     nlb			: unpack(rg_command.cdw12[15:0]),
		     logical_block_address	: unpack({rg_command.cdw11, rg_command.cdw10})
		  });
		  rg_execution_state <= IDLE;
	       end
      endcase
   endrule
   /* End of the Execution state machine */


      /* Begin of Data Structure state machine. */
   rule rl_data_structure_idle_state (rg_data_structure_state == IDLE);
      /* Get the next data structure request from the Data Transfer FIFO.

      This rule implicitly fires only if ff_data_structure_info is not
      empty.
      */
      $display("%d: NvmController: New data structure request.", $stime());
      rg_read_Bram_Count <= 0;
      rg_local_data_structure_info <= ff_data_structure_info.first();
      ff_data_structure_info.deq();
      rg_data_structure_state <= TRANSMIT;
   endrule: rl_data_structure_idle_state

   rule rl_data_structure_transmit_state (rg_data_structure_state == TRANSMIT);
      /* Request the data from BRAM. */
      //$display("%d: NvmController: Request data structure %d from BRAM %d.",
      //	$stime(), rg_local_data_structure_info.cns, rg_read_Bram_Count);
      case (rg_local_data_structure_info.cns)
	 IDENTIFY_NAMESPACE:
	 namespace_data_structure.portA.request.put(BRAMRequest{
	 write			: False,
	 address		: pack(rg_read_Bram_Count)[(15 - valueOf(TLog#(`WDC))) - 1:0],
	 datain			: ?,
	 responseOnWrite	: False
	 });
	 IDENTIFY_CONTROLLER:
	 controller_data_structure.portA.request.put(BRAMRequest{
	 write			: False,
	 address		: pack(rg_read_Bram_Count)[(15 - valueOf(TLog#(`WDC))) - 1:0],
	 datain			: ?,
	 responseOnWrite	: False
	 });
	 LBA_RANGE_TYPE:
	 lbaRange_data_structure.portA.request.put(BRAMRequest{
	 write			: False,
	 address		: pack(rg_read_Bram_Count)[(15 - valueOf(TLog#(`WDC))) - 1:0],
	 datain			: ?,
	 responseOnWrite	: False
	 });
      endcase
      if (rg_read_Bram_Count <
	  fromInteger(valueOf(TSub#(TDiv#(TMul#(4096,8),`WDC), 1))))
	 begin
	    /* Increment till end of 4KB file; reset in IDLE to avoid more requests
	    if the other rules of the TRANSMIT state take a longer time. */
	    rg_read_Bram_Count <= rg_read_Bram_Count + 1;
	 end
   endrule: rl_data_structure_transmit_state

   rule rl_data_structure_transmit_state1 (rg_data_structure_state == TRANSMIT);
      /* Request acces for the PCIe Interface and hold the request and thus the
      grant (sticky) during the transmission */
      rr_pcie.clients[1].request;
   endrule: rl_data_structure_transmit_state1

   rule rl_data_structure_transmit_state_identify_namespace
      (rg_data_structure_state == TRANSMIT &&
       rg_local_data_structure_info.cns == IDENTIFY_NAMESPACE &&
       fn_rr_pcie_id == 1);
      /* Send the response data from BRAM to the PCIe Interface if it is an
      `IDENTIFY_NAMESPACE` request. */
      if (dwr_wait == 1'b0) begin
	 //$display("%d: NvmController: Send identify namespace data structure.",
	 //		$stime());
	 let lv_data <- namespace_data_structure.portA.response.get();
	 rg_out_data_to_pcie <= lv_data;
	 rg_out_write_to_pcie <= 1;
	 rg_out_tag_to_pcie <= 'd0;
	 rg_out_payload_length <= 'd1024;
	 rg_out_address_to_pcie <= rg_local_data_structure_info.prp1_address +
	 extend(rg_data_structure_count) * (`WDC / 8);
	 drg_out_data_valid <= 1; // Sending Valid data

	 if (rg_data_structure_count ==
	     fromInteger(valueOf(TSub#(TDiv#(TMul#(4096,8),`WDC), 1))))
	    begin
	       /* all data transfered -> request completion and return to IDLE */
	       rg_data_structure_count <= 0;
	       $display("%d: NvmController: Send identify namespace completion.",
			$stime());
	       ff_data_structure_completion.enq(Completion_info_type {
	       dword0		: 0,
	       status		: SUCCESS,
	       sqid		: rg_local_data_structure_info.sqid,
	       command_id	: rg_local_data_structure_info.command_id
	       });
	       rg_data_structure_state <= IDLE;
	    end else begin
			rg_data_structure_count <= rg_data_structure_count + 1;
		     end
      end else if (rg_data_structure_count > 0)
	     begin
		drg_out_data_valid <= 1; // Sending Valid data
	     end
   endrule: rl_data_structure_transmit_state_identify_namespace


   rule display_1;
      $display("%d: NvmController: fn_rr_pcie_id %d, dwr_wait: %d, rg_data_transfer_state[0]: %d",
	 $stime(), fn_rr_pcie_id, dwr_wait, rg_data_transfer_state[0]);
   endrule


   rule rl_data_structure_transmit_state_identify_controller
      (rg_data_structure_state == TRANSMIT &&
       rg_local_data_structure_info.cns == IDENTIFY_CONTROLLER &&
       fn_rr_pcie_id == 1);
      if (dwr_wait == 1'b0) begin
	 /* Send the response data from BRAM to the PCIe Interface if it is an
	 `IDENTIFY_CONTROLLER` request. */
	 //$display("%d: NvmController: Send identify controller data structure %d.",
	 //		$stime(), rg_data_structure_count);
	 let lv_data <- controller_data_structure.portA.response.get();
	 rg_out_data_to_pcie <= lv_data;
	 rg_out_write_to_pcie <= 1;
	 rg_out_tag_to_pcie <= 'd0;
	 rg_out_payload_length <= 'd1024;
	 rg_out_address_to_pcie <= rg_local_data_structure_info.prp1_address +
	 extend(rg_data_structure_count) * (`WDC / 8);
	 drg_out_data_valid <= 1; // Sending Valid data

	 if (rg_data_structure_count ==
	     fromInteger(valueOf(TSub#(TDiv#(TMul#(4096,8),`WDC), 1))))
	    begin
	       /* all data transfered -> request completion and return to IDLE */
	       rg_data_structure_count <= 0;
	       $display("%d: NvmController: Send identify controller completion.",
			$stime());
	       ff_data_structure_completion.enq(Completion_info_type {
	       dword0		: 0,
	       status		: SUCCESS,
	       sqid		: rg_local_data_structure_info.sqid,
	       command_id	: rg_local_data_structure_info.command_id
	       });
	       rg_data_structure_state <= IDLE;
	    end else
		   begin
		      rg_data_structure_count <= rg_data_structure_count + 1;
		   end
      end else if (rg_data_structure_count > 0)
	     begin
		drg_out_data_valid <= 1; // Sending Valid data
	     end
   endrule: rl_data_structure_transmit_state_identify_controller

   /*
   rule display;
   $display("%d: NvmController: %d, %d", $stime(), fn_rr_pcie_id, dwr_wait);
   endrule
   */

   rule rl_data_structure_transmit_state_lba_range_type
      (rg_data_structure_state == TRANSMIT &&
       rg_local_data_structure_info.cns == LBA_RANGE_TYPE &&
       fn_rr_pcie_id == 1);
      /* Send the response data from BRAM to the PCIe Interface if it is an
      `LBA_RANGE_TYPE` request. */
      if (dwr_wait == 1'b0) begin
	 let lv_data <- lbaRange_data_structure.portA.response.get();
	 rg_out_data_to_pcie <= lv_data;
	 rg_out_write_to_pcie <= 1;
	 rg_out_tag_to_pcie <= 'd0;
	 rg_out_payload_length <= 'd1024;
	 rg_out_address_to_pcie <= rg_local_data_structure_info.prp1_address +
	 extend(rg_data_structure_count) * (`WDC / 8);
	 drg_out_data_valid <= 1; // Sending Valid data

	 if (rg_data_structure_count ==
	     fromInteger(valueOf(TSub#(TDiv#(TMul#(4096,8),`WDC), 1))))
	    begin
	       /* all data transfered -> request completion and return to IDLE */
	       rg_data_structure_count <= 0;
	       ff_data_structure_completion.enq(Completion_info_type {
		  dword0	: rg_feature[0].lbaRangeType,
		  status	: SUCCESS,
		  sqid		: rg_local_data_structure_info.sqid,
		  command_id	: rg_local_data_structure_info.command_id
								      });
	       rg_data_structure_state <= IDLE;
	    end 
	 else 
	    begin
	       rg_data_structure_count <= rg_data_structure_count + 1;
	    end
      end 
      else if (rg_data_structure_count > 0)
	 begin
	    drg_out_data_valid <= 1; // Sending Valid data
	 end
   endrule: rl_data_structure_transmit_state_lba_range_type
   /* End of Data Structure state machine. */

   /* Begin of Completion state machine. */
   Reg#(UInt#(2)) rg_completion_count <- mkReg(0);  // TODO TODO SIZE ADJ

   rule rl_completion_idle_state0 (rg_completion_state == IDLE &&
      ff_completion.notEmpty());
      /* Request PCIe access if the completion queue is not empty.

      This rule fires only if ff_completion is not empty.
      */
      rg_local_completion <= fn_build_completion(ff_completion.first());
      rg_local_completion_cqid <= rg_cqIDofSQ[ff_completion.first().sqid];
      ff_completion.deq();
      rg_completion_state <= SEND;
   endrule: rl_completion_idle_state0

   for (Integer i = 0; i < `NO_CHANNELS; i = i + 1) begin
      rule rl_completion_idle_state1(rg_completion_state == IDLE &&
	 ff_data_transfer_completion[i].notEmpty());
	 /* Request PCIe access if the data transfer completion queue is not
	 empty.

	 This rule fires only if ff_data_transfer_completion is not empty.
	 */
	 $display("%d: NvmController: ff_data_transfer_completion[%d]",
		  $stime(), i);
	 rg_local_completion <= fn_build_completion(
	    ff_data_transfer_completion[i].first());
	 rg_local_completion_cqid <=
	 rg_cqIDofSQ[ff_data_transfer_completion[i].first().sqid];
	 ff_data_transfer_completion[i].deq();
	 rg_completion_state <= SEND;
      endrule: rl_completion_idle_state1
   end

   rule rl_completion_idle_state2 (rg_completion_state == IDLE &&
      ff_data_structure_completion.notEmpty());
      /* Request PCIe access if the completion queue is not empty.

      This rule fires only if ff_data_structure_completion is not empty.
      */
      $display("%d: NvmController: New data structure completion request.",
	       $stime());
      rg_local_completion <= fn_build_completion(
	 ff_data_structure_completion.first());
      rg_local_completion_cqid <=
      rg_cqIDofSQ[ff_data_structure_completion.first().sqid];
      ff_data_structure_completion.deq();
      rg_completion_state <= SEND;
   endrule: rl_completion_idle_state2


   rule rl_completion_send_state0 (rg_completion_state == SEND);
      /* Hold the request until it is granted and during transmission. */
      $display ("%d: NvmController: Completion requesting for PCIe access\n",$stime());
      rr_pcie.clients[0].request();
   endrule: rl_completion_send_state0

   rule rl_completion_send_state1 (rg_completion_state == SEND &&
      (fn_rr_pcie_id == 0));
      /* Send data to PCIe Interface when access is granted. */
      $display("%d: completion cqid: %d (dwr_wait: %d)", $stime(),
	       rg_local_completion_cqid, dwr_wait);  // TODO TODO TODO check this; dwr_wait is always 1 after two dwords
      if (dwr_wait == 0) begin
	 drg_out_data_valid <= 1;
	 // set PCIe output registers
	 rg_out_write_to_pcie <= 1;
	 rg_out_tag_to_pcie <= 'd0;
	 rg_out_payload_length <= 'd4;
	 // get the corresponding completion queue for the sqid of this command
	 let lv_cqID = rg_local_completion_cqid;  // TODO remove renaming
	 $display("%d: completion: cqid: %d, pcie address: 0x%x", $stime(),
		  lv_cqID, rg_cq_base_address[lv_cqID] + extend(cqtdbl[lv_cqID]) *
		  `CQ_ENTRY_SIZE);

	 rg_out_address_to_pcie <=
	 rg_cq_base_address[lv_cqID] +
	 extend(cqtdbl[lv_cqID]) * `CQ_ENTRY_SIZE +
	 extend(rg_completion_count) * (`WDC / 8);
	 Vector#(TDiv#(TMul#(16,8), `WDC), Bit#(`WDC)) lv_completion_pack =
	 unpack(pack(rg_local_completion));
	 rg_out_data_to_pcie <= lv_completion_pack[rg_completion_count];

	 if (rg_completion_count ==
	     fromInteger(valueOf(TSub#(TDiv#(TMul#(16,8), `WDC), 1))))
	    begin
	       /* last part sent */
	       rg_completion_count <= 0;
	       // update interrupts
	       /*
	       The register rg_cqID must be updated while executing the
	       particular Command in the associated SQ
	       // TODO what does this mean?
	       */
	       if(rg_InterruptEnable[lv_cqID] == 1'b1) begin
		  let vector_number = rg_InterruptVector[lv_cqID];
		  $display("%d: interrupt for vector %d", $stime(), vector_number);
		  dwr_compl_int <= 1 << vector_number;
	       end
	       
	       // update cqtdbl and phase tag
	       if (cqtdbl[lv_cqID] == rg_cq_size[lv_cqID]) begin
		  cqtdbl[lv_cqID] <= 0;
		  rg_phase_tags[lv_cqID] <= ~rg_phase_tags[lv_cqID];
	       end else begin
			   cqtdbl[lv_cqID] <= cqtdbl[lv_cqID] + 1;
			end
	       rg_completion_state <= IDLE;
	    end 
	 else 
	    begin
	       rg_completion_count <= rg_completion_count + 1;
	    end
      end
      else if (rg_completion_count > 0)
	 begin
	    drg_out_data_valid <= 1;
	 end
   endrule: rl_completion_send_state1
   /* End of Completion state machine. */

   /* Begin of PRP Transfer state machine. */
   /*
   maximum of 513 prp addresses supported (PRP1 + PRP2 List)
   */
   // TODO TODO parametrize max_prp_length
   Reg#(UInt#(TLog#(513))) rg_prp_counter <- mkReg(0);
   Reg#(UInt#(TLog#(513))) rg_data_prp_counter <- mkReg(0);
   Wire#(Ftl_cmd) wr_send_ftl_cmd <- mkWire();
   Wire#(UInt#(64)) wr_send_ftl_prp <- mkWire();
   Wire#(Bool) wr_ftl_cmd_busy <- mkWire();
   Wire#(Bool) wr_ftl_prp_busy <- mkWire();
 
   
   rule rl_prp_transfer_send_cmd (rg_prp_transfer_state == IDLE && !wr_ftl_cmd_busy);
      $display("%d: NvmController: PRP Transfer State Machine: NLB %d", $stime(),
	 ff_data_transfer_info.first().nlb);
      /*send cmd to FTL proc*/
      
      Nand_cmd_opcode lv_opcode = ff_data_transfer_info.first().nand_cmd_opcode;
      $display ("%d: NvmController: PRP Transfer State : opcode: %d length %d tag: %d\n",
	 lv_opcode, ff_data_transfer_info.first().nlb, ff_data_transfer_info.first().command_id);
      wr_send_ftl_cmd <= Ftl_cmd {
	 opcode		: lv_opcode,
	 length		: pack(ff_data_transfer_info.first().nlb),
	 tag		: {pack(ff_data_transfer_info.first().sqid), 
			   pack(ff_data_transfer_info.first().command_id)},
	 nand_lba	: ff_data_transfer_info.first().logical_block_address
	 };
      
      if (lv_opcode == READ_NAND || lv_opcode == WRITE_NAND)
	 begin
	    rg_prp_transfer_state <= SEND_PRP1;
	 end
   endrule : rl_prp_transfer_send_cmd
   
   rule rl_prp_transfer_send_prp1 (rg_prp_transfer_state == SEND_PRP1 && !wr_ftl_prp_busy);
      $display ("%d: Nvme: PRP transfer state machine: %d: sending prp1 to map",$stime()); 
      
      /*send prp1 to FTL proc*/
      wr_send_ftl_prp <= ff_data_transfer_info.first().prp1_address;
      
      if (ff_data_transfer_info.first().nlb == 0)
	 begin
	    $display ("%d: NvmController: PRP transfer state machine: going back to IDLE",$stime());
	    rg_prp_transfer_state <= IDLE;
	    ff_data_transfer_info.deq();
	 end
      else if (ff_data_transfer_info.first().nlb == 1)
	 begin
	    $display ("%d: NvmController: PRP transfer state machine: going to SEND_PRP2",$stime());
	    rg_prp_transfer_state <= SEND_PRP2;
	 end
      else 
	 begin
	    $display ("%d: NvmController: PRP transfer state machine: going to REQUEST_PRP_LIST",$stime());
	    rg_prp_transfer_state <= REQUEST_PRP_LIST;
	 end
      
   endrule : rl_prp_transfer_send_prp1
   
   // once the ftl_request_busy is asserted to be false, it should not block until
   // the requests correspoding to length specified in the ftl_cmd is received.
   
   rule rl_prp_transfer_send_prp2 (rg_prp_transfer_state == SEND_PRP2 && !wr_ftl_prp_busy);
      
      $display ("%d: NvmController: Sending PRP2 to map\n",$stime());
      wr_send_ftl_prp <= ff_data_transfer_info.first().prp2_address;
      rg_prp_transfer_state <= IDLE;
      ff_data_transfer_info.deq();
      
   endrule : rl_prp_transfer_send_prp2
   
   rule rl_prp_transfer_request_pcie_state0 (rg_prp_transfer_state == REQUEST_PRP_LIST);
      /* Hold PCIe access request until it is granted */
      rr_pcie.clients[3].request();
   endrule : rl_prp_transfer_request_pcie_state0
   
   rule rl_prp_transfer_request_pcie_state1 (rg_prp_transfer_state == REQUEST_PRP_LIST &&
      fn_rr_pcie_id == 3 && dwr_wait == 1'b0);
      /* Request to read the data from PCIe. */
      rg_out_write_to_pcie <= 0;
      rg_out_tag_to_pcie <= 2;
      rg_out_payload_length <= 2*extend(pack(ff_data_transfer_info.first().nlb));
      rg_out_address_to_pcie <= ff_data_transfer_info.first().prp2_address;
      drg_out_data_valid <= 1;
      
      // reset counter for next state
      rg_prp_counter <= 0;
      rg_prp_transfer_state <= GET_PRP_LIST;
      
   endrule : rl_prp_transfer_request_pcie_state1
     
   `ifdef WDC_32
   rule rl_prp_transfer_get_pcie_state (rg_prp_transfer_state == GET_PRP_LIST &&
      !wr_ftl_prp_busy);
      $display("%d: NvmController: get_prp_list %d - %d -- PRP Address: 0x%x",
	       $stime(), rg_prp_counter, rg_local_prp_list_count,
	       ff_prp_transfer_data.first());
      
      if (rg_local_prp_list_count == 0)
	 begin
	    rg_local_prp_list_tmp <= ff_prp_transfer_data.first();
	    ff_prp_transfer_data.deq();
	    rg_local_prp_list_count <= 1;
	 end
      else
	 begin
	    rg_local_prp_list_count <= 0;
	    wr_send_ftl_prp <= unpack({pack(ff_prp_transfer_data.first()),
	       rg_local_prp_list_tmp});
	    ff_prp_transfer_data.deq();
	    if (rg_prp_counter == truncate(ff_data_transfer_info.first().nlb) - 1)
	       begin
		  rg_prp_counter <= 0;
		  rg_prp_transfer_state <= IDLE;
		  ff_data_transfer_info.deq();
	       end
	    else
	       begin
		  rg_prp_counter <= rg_prp_counter + 1;
	       end
	 end
      endrule : rl_prp_transfer_get_pcie_state
   
   `else
   rule rl_prp_transfer_get_pcie_state (rg_prp_transfer_state == GET_PRP_LIST &&
      !wr_ftl_prp_busy);
      $display("%d: NvmController: get_prp_list %d (of %d) - %d -- PRP Address: 0x%x",
	       $stime(), rg_prp_counter, ff_data_transfer_info.first().nlb,
	       rg_local_prp_list_count, ff_prp_transfer_data.first());
      
      //TODO might want to send two address at once to ftl proc.
      UInt#(TLog#(TMul#(TDiv#(`WDC, 64), 64))) lv_low_bound = 64 * (
	 extend(rg_local_prp_list_count));
      `ifdef WDC_64
      wr_send_ftl_prp <=  unpack(pack(ff_prp_transfer_data.first())
	 [63 : 0]);
      `else
      wr_send_ftl_prp <=  unpack(pack(ff_prp_transfer_data.first())
	 [lv_low_bound + 63 : lv_low_bound]);
      `endif
      if (rg_prp_counter == truncate(ff_data_transfer_info.first().nlb) - 1)
	 begin
	    $display ("-----%d: NvmController: PRP transfer complete-----",$stime());
	    rg_prp_counter <= 0;
	    rg_local_prp_list_count <= 0;
	    ff_prp_transfer_data.deq();
	    ff_data_transfer_info.deq();
	    rg_prp_transfer_state <= IDLE;
	 end
      else
	 begin
	    if (rg_local_prp_list_count == fromInteger(valueOf(TSub#(TDiv#(`WDC,64),1))))
	       begin
		rg_local_prp_list_count <= 0;
		ff_prp_transfer_data.deq();
	       end
	    else
	       begin
		  rg_local_prp_list_count <= rg_local_prp_list_count + 1;
	       end
	    rg_prp_counter <= rg_prp_counter + 1;
	 end
   endrule : rl_prp_transfer_get_pcie_state
   `endif

   /* begin of data transfer state machine */
   Reg#(Bool) rg_read_nand_req_pcie <- mkReg(False);
   Vector#(`NO_CHANNELS,FIFO#(Nand_cmd)) ff_ftl_nand_cmd <- replicateM(mkSizedFIFO(5));
   Vector#(`NO_CHANNELS,FIFO#(Ftl_prp_tag)) ff_ftl_prp <- replicateM(mkSizedFIFO(5));
   Vector#(`NO_CHANNELS,Reg#(Bit#(16))) rg_req_length_counter <- replicateM(mkReg(0));
   Vector#(`NO_CHANNELS, Reg#(UInt#(TLog#(TDiv#(TMul#(4096, 8), `WDC)))))
   rg_data_transfer_counter <- replicateM(mkReg(0));

   //TODO TODO request for pcie one cycle before
   for (Integer i = 0; i < `NO_CHANNELS; i = i+1) begin
      rule rl_data_transfer_req_pcie0 (rg_data_transfer_state[i] == IDLE &&
	 ff_ftl_nand_cmd[i].first().opcode == WRITE_NAND);
	 $display ("%d: NvmController: data transfer state machine %d requesting pcie access",
	    $stime(), i);
	 rr_pcie.clients[4+i].request();
      endrule : rl_data_transfer_req_pcie0
      
      rule rl_data_transfer_req_pcie (rg_data_transfer_state[i] == IDLE && 
	 ff_ftl_nand_cmd[i].first().opcode == WRITE_NAND &&
	 fn_rr_pcie_id == (4 + fromInteger(i)) && dwr_wait == 1'b0);
	 $display ("%d: NvmController: data transfer state machine %d got pcie grant",$stime());
	 rg_out_write_to_pcie <= 0;
	 rg_out_tag_to_pcie <= 3 + fromInteger(i);
	 rg_out_payload_length <= 'd1024;
	 rg_out_address_to_pcie <= ff_ftl_prp[i].first().main_mem_addr;
	 drg_out_data_valid <= 1;
	 rg_data_transfer_state[i] <= WRITE_NAND;
      endrule : rl_data_transfer_req_pcie
      
      rule rl_data_transfer_read_nand0 (rg_data_transfer_state[i] == READ_NAND &&
	 rg_read_nand_req_pcie);
	 rr_pcie.clients[4+i].request();
      endrule : rl_data_transfer_read_nand0
      
      // The rest of the state machine is handled in ifc_nand_flash interface methods
   end
   
   
   /* Interrupt debug rule */
   rule rl_interrupt_debug;
      if (dwr_compl_int != 0)
	 $display ("%d: completion interrupt asserted by completion state machine\n",$stime());
   endrule : rl_interrupt_debug
   
   /* Begin rules for Interrupt generation. */
   rule rl_vect_num;
      $display ("\n");
      let lv_event_d =
      (status_reg & ~mask_reg & dwr_clear_mask & ~dwr_set_mask) |
      (~status_reg & dwr_compl_int & ~mask_reg) |
      (~status_reg & ~mask_reg & dwr_clear_mask & ~dwr_set_mask & dwr_compl_int);
      
      // $display ("%d: NvmController: Interrupt rule fired\n",$stime());
      //status_reg <= status_reg | dwr_compl_int;
      mask_reg <= mask_reg & ~dwr_clear_mask | dwr_set_mask;
      Maybe#(UInt#(TLog#(32))) lv_event = tagged Invalid;
      for (Integer i = 4; i >= 0; i = i - 1) begin
	 if (lv_event_d[i] == 1)
	    lv_event = tagged Valid fromInteger(i);
      end
      if (!isValid(lv_event)) begin
	 vectr <= 0;
	 vectr_rdy <= 0;
      end
      else if (fromMaybe(?, lv_event) == 0) begin
	 /* Admin Submission Queue interrupt is not aggregated */
	 vectr <= 0;
	 vectr_rdy <= 1;
	 status_reg[0] <= 0;
      end
      else begin
	 let lv_val = fromMaybe(?, lv_event);
	 status_reg[lv_val] <= 0;
	 if(vectr_count[lv_val - 1] == aggr_threshold) begin
	    $display("%d: NvmController: Interrupt for vector %d sent.",
		     $stime(), lv_val);
	    vectr <= pack(lv_val);
	    vectr_rdy <= 1;
	    vectr_count[lv_val - 1] <= 0;
	 end
	 else begin
	    vectr_rdy <= 0;
	    $display(" Aggregated vector number %d .... %d ", lv_val,
		     vectr_count[lv_val - 1]);
	    vectr_count[lv_val - 1] <= vectr_count[lv_val - 1] + 1;
	 end
      end
   endrule
   /* End rules for interrupt generation. */

   rule rl_ns_count;
      /* Rule for sqid1 execution time counter used by software.
      
      100 MHz clock is assumed and it only works if only one command is issued at
      the same time.
      */
      if (rg_count_started) begin
	 if (rg_completion_state == SEND) begin
	    $display("%d: NvmController: Stop counter.", $stime());
	    rg_count_started <= False;
	 end else begin
		     rg_ns_count <= rg_ns_count + 10;
		  end
      end
      else if (rr_arb.clients[1].grant()) begin
	 $display("%d: NvmController: Start counter.", $stime());
	 rg_ns_count <= 0;
	 rg_count_started <= True;
      end
   endrule: rl_ns_count

   
   Vector#(`NO_CHANNELS,Ifc_ftl_processor_out) arr_ftl_proc_out_ifc;
   for (Integer i = 0; i < `NO_CHANNELS; i = i+1) begin
      arr_ftl_proc_out_ifc[i] = (
	 interface Ifc_ftl_processor_out;
	 method Action _put_cmd (Nand_cmd _nand_cmd);
	    ff_ftl_nand_cmd[i].enq(_nand_cmd);
	    $display ("%d: NvmController: Cmd from ftl received",$stime());
	 endmethod
	 method Action _put_prp (Ftl_prp_tag _prp_tag);
	    $display ("%d: NvmController: Map from ftl received",$stime());
	    ff_ftl_prp[i].enq(_prp_tag);
	 endmethod
	 endinterface
	 );
   end

   
   Vector#(`NO_CHANNELS, Ifc_nand_flash) arr_nand_flash_ifc;
   for (Integer i = 0; i < `NO_CHANNELS; i = i+1) begin
      rule rl_nand_data_in_hold(
	 rg_data_transfer_state[i] == READ_NAND &&
	 (fn_rr_pcie_id == (4 + fromInteger(i))) &&
	 dwr_wait == 1'b1 &&
	 rg_data_transfer_counter[i] > 0
	 );
	 
	 /* Hold the valid signal during PCIe wait. */
	 $display("%d: NvmController: WRITE_PCIE: hold valid", $stime());
	 drg_out_data_valid <= 1;
      endrule: rl_nand_data_in_hold
      
      arr_nand_flash_ifc[i] = 
      (
       interface Ifc_nand_flash;
	  method ActionValue#(Nand_request_struct) request_address_() if (
	     rg_data_transfer_state[i] == IDLE &&
	     ff_ftl_nand_cmd[i].first().opcode == READ_NAND);
	     
	     $display ("%d: NvmController: request NAND read address: 0x%x length: 0x%x",$stime(),
		ff_ftl_nand_cmd[i].first().nand_pba/4096, ff_ftl_nand_cmd[i].first().length + 1);
	    rg_data_transfer_state[i] <= READ_NAND;
	     return Nand_request_struct {
		address : unpack(truncate(pack(ff_ftl_nand_cmd[i].first().nand_pba))/4096),
		length  : pack(ff_ftl_nand_cmd[i].first().length + 1)
		};
	  endmethod : request_address_
	  
	  method Action _data_in(Bit#(`WDC) _data) if (
	     rg_data_transfer_state[i] == READ_NAND &&
	     fn_rr_pcie_id == (4 + fromInteger(i)) &&
	     dwr_wait == 1'b0);
	     $display("%d: NvmController: WRITE_PCIE: received _data: 0x%x (%d)",
		      $stime(), _data, rg_data_transfer_counter[i]);
	     rg_out_write_to_pcie <= 1;
	     rg_out_tag_to_pcie <= 'd0;
	     rg_out_payload_length <= 'd1024;
	     rg_out_address_to_pcie <= ff_ftl_prp[i].first().main_mem_addr + 
		extend(rg_data_transfer_counter[i])*fromInteger(valueOf(TDiv#(`WDC,8)));
	     rg_out_data_to_pcie <= _data;
	     drg_out_data_valid <= 1;
	     if (rg_data_transfer_counter[i] == fromInteger(valueOf(TSub#(TDiv#(TMul#(4096,8),`WDC),1))))
		begin
		   rg_data_transfer_counter[i] <= 0;
		   ff_ftl_prp[i].deq();
		   if (ff_ftl_prp[i].first().tag matches tagged Valid .tag)
		      begin
			 ff_data_transfer_completion[i].enq(Completion_info_type{
			    dword0	: 0,
			    status	: SUCCESS,
			    sqid	: unpack(tag[31:16]),
			    command_id	: unpack(tag[15:0])
			    });
		      end
		   if (rg_req_length_counter[i] == ff_ftl_nand_cmd[i].first().length())
		      begin
			 rg_data_transfer_state[i] <= IDLE;
			 rg_read_nand_req_pcie <= False;
			 rg_req_length_counter[i] <= 0;
			 ff_ftl_nand_cmd[i].deq();
		      end
		   else begin
		      rg_req_length_counter[i] <= rg_req_length_counter[i] + 1;
		   end
		end
	     else begin
		rg_data_transfer_counter[i] <= rg_data_transfer_counter[i] + 1;
	     end
	  endmethod : _data_in

 
	  method ActionValue#(Nand_write_struct) data_out_ if (
	     rg_data_transfer_state[i] == WRITE_NAND);
	     /* Write the data to the NAND Flash which are received as a
	     completion.
	     
	     This rule implicitly only fires when ff_data_transfer_data is
	     not empty and thus data has been received from PCIe.
	     */
	     $display("%d: NvmController: Try to write data to NAND. Physical Block Address: 0x%x (%d -- %d)",
		$stime(),ff_ftl_nand_cmd[i].first().nand_pba,rg_data_transfer_counter[i], rg_req_length_counter[i]);
	     if (rg_data_transfer_counter[i] == fromInteger(valueOf(TSub#(TDiv#(TMul#(4096,8),`WDC),1))))
		begin
		   rg_data_transfer_counter[i] <= 0;
		   ff_ftl_prp[i].deq();
		   rg_data_transfer_state[i] <= IDLE;
		   if (ff_ftl_prp[i].first().tag matches tagged Valid .tag)
		      begin
			 ff_data_transfer_completion[i].enq(Completion_info_type{
			    dword0	: 0,
			    status	: SUCCESS,
			    sqid	: unpack(tag[31:16]),
			    command_id	: unpack(tag[15:0])
			    });
		      end
		   if (rg_req_length_counter[i] == ff_ftl_nand_cmd[i].first().length())
		      begin
			 rg_req_length_counter[i] <= 0;
			 ff_ftl_nand_cmd[i].deq();
		      end
		   else begin
		      rg_req_length_counter[i] <= rg_req_length_counter[i] + 1;
		   end
		end
	     else begin
		rg_data_transfer_counter[i] <= rg_data_transfer_counter[i] + 1;
	     end
	     ff_data_transfer_data[i].deq();
	     return Nand_write_struct {
		address : unpack(truncate(pack(ff_ftl_nand_cmd[i].first().nand_pba/4096))),
		data    : ff_data_transfer_data[i].first(),
		length  : unpack(ff_ftl_nand_cmd[i].first().length) + 1
		};
       endmethod : data_out_
       
       
       method Action _interrupt() if (rg_data_transfer_state[i] == READ_NAND);
	  $display("%d: NvmController: READ_NAND: Interrupt from NAND Flash received",
		   $stime());
	  rg_read_nand_req_pcie <= True;
       endmethod : _interrupt

       method bit _enable();
	  return 1'b1;
       endmethod: _enable

       method Action _busy(bit _is_busy);
       endmethod: _busy

       endinterface
       );
   end
   
   interface ifc_nand_flash = arr_nand_flash_ifc;
   
   interface Ifc_config ifc_config;
      method Action _write(Bit#(32) _address, Bit#(64) _data,Bool dword_aligned);
	 /* Method for writing into the Controller registers */
	 $display("%d: ifc_config._write: Configuration Write of 0x%x to 0x%x",
		  $stime(), _data, _address);
	 case (_address)
	    32'h0c:
	    dwr_set_mask <= _data[31:0];
	    32'h10:
	    dwr_clear_mask <= _data[31:0];
	    32'h14:
	    cc <= Controller_configuration {
	       reserved1	: _data[31:24],
	       iocqes		: _data[23:20],
	       iosqes		: _data[19:16],
	       shn		: _data[15:14],
	       ams		: _data[13:11],
	       mps		: _data[10:7],
	       css		: _data[6:4],
	       reserved2	: _data[3:1],
	       en		: _data[0]
	       };
	    32'h24: begin
		       // set admin submission queue size
		       rg_sq_size[0] <= unpack({0, _data[11:0]});
		       // set admin completion queue size
		       rg_cq_size[0] <= unpack({0, _data[27:16]});
		    end
	    32'h28: begin
		       if (dword_aligned) begin
			  asq <= ASQ {
			     asqb		: {asq.asqb[51:20], _data[31:12]},
			     reserved		: 0
			     };
		       end
		       else
			  begin
			     asq <= ASQ {
				asqb		: _data[63:12],
				reserved	: 0
				};
			  end
		    end
	    32'h2c				:
	    asq <= ASQ {
			asqb			: {_data[63:32], asq.asqb[19:0]},
			reserved		: 0
			};
	    32'h30				: begin
		       if (dword_aligned) begin
			  acq <= ACQ {
			     acqb		: {acq.acqb[51:20], _data[31:12]},
			     reserved		: 0
			     };
		       end
		       else
			  begin
			     acq <= ACQ {
				acqb		: _data[63:12],
				reserved	: 0
				};
			  end
		    end
	    32'h34				:
	    acq <= ACQ {
	       acqb				: {_data[63:32], acq.acqb[19:0]},
	       reserved				: 0
			};
	    32'h1FFF:
	    begin
	    `ifdef DEBUG_ENABLE
	    rg_out_leds2 <= _data[7:0];
	    `endif
	    end
	    default: 
	    begin
	       for (Integer y = 0; y < `No_SQueues; y = y + 1)	begin
		  if (_address == 32'h1000 + ((2 * fromInteger(y)) *
					      (4 << lv_cap.dstrd)))
		     begin
			sqtdbl[y] <= SQTDBL {
			   reserved	: _data[31:16],
			   sqt		: unpack(_data[15:0])
			   };
			$display("%d: ifc_config._write: SQTail door bell updated %b ",
				 $stime(), _data);
		     end
		  else if (_address == 32'h1000 + ((2 * fromInteger(y) + 1) *
						   (4 << lv_cap.dstrd)))
		     begin
			cqhdbl[y] <= CQHDBL {
			   reserved	: _data[31:16],
			   cqh		: unpack(_data[15:0])
			   };
		     end
	       end
	    end
	 endcase
      endmethod: _write


      method Bit#(64) read_(Bit#(32) address);
      /* Method for reading from the Controller registers */
	 case (address)
	    32'h00:
	    return {8'd0, lv_cap.mpsmax, lv_cap.mpsmin, 7'd0, lv_cap.css, 1'd0,
		    lv_cap.dstrd, lv_cap.to, 5'd0, lv_cap.ams, lv_cap.cqr, lv_cap.mqes };
	    32'h04:
	    return {32'h0, 8'd0, lv_cap.mpsmax, lv_cap.mpsmin, 7'd0, lv_cap.css, 1'd0,
		    lv_cap.dstrd};
	    32'h08:
	    return {32'd0, version.mjr , version.mnr };
	    32'h0c:
	    return {32'd0, mask_reg}; // return the internal Mask register value
	    32'h10:
	    return {32'd0, mask_reg}; // return the internal Mask Register Value
	    32'h14:
	    return {32'd0, 8'd0, cc.iocqes, cc.iosqes, cc.shn, cc.ams, cc.mps,
		    cc.css, 3'd0, cc.en};
	    32'h1c:
	    return {32'd0, 28'd0, csts.shst, csts.cfs, csts.rdy};
	    32'h24:
	    return {32'd0, 4'd0, pack(rg_cq_size[0])[11:0], 4'd0,
		    pack(rg_sq_size[0])[11:0]};
	    32'h28:
	    return {asq.asqb, 12'd0};
	    32'h2c:
	    return {32'h0, asq.asqb[51:20]};
	    32'h30:
	    return {acq.acqb, 12'd0};
	    32'h34:
	    return {32'h0, acq.acqb[51:20]};
	    32'h38:
	    return extend(rg_debug_nvm_command.opcode);
	    32'h3c:
	    return extend(pack(rg_debug_nvm_command.command_id));
	    32'h40:
	    return extend(pack(rg_debug_nvm_command.prp1));
	    32'h44:
	    return extend(pack(rg_debug_nvm_command.prp2));
	    32'h1FFF:
	    return rg_ns_count;
	    default:
	    begin
	       /*
	       Check if the Submission Queue Tail Doorbell register or the
	       Completion Queue Head Doorbell register is to be returned.  For the
	       calculations please refer to the NVM Specification 1.1 section
	       3.1.11 and 3.2.11.
	       */
	       let lv_sq_low_addr = 32'h1000;
	       let lv_cq_high_addr = 32'h1000 + (2 * (`No_SQueues - 1) + 1) *
	       (4 << lv_cap.dstrd);
	       if (address >= lv_sq_low_addr && address <= lv_cq_high_addr) begin
		  /* the address is in the doorbell register range */
		  let lv_index = ((address - 32'h1000) / (4 << lv_cap.dstrd));
		  if (lv_index[0] == 1'b0) begin
		     /* it is a submission queue tail doorbell address */
		     return {32'd0, 16'd0, pack(sqtdbl[lv_index / 2].sqt)};
		  end
		  else
		     begin
			/* it is a completion queue head doorbell address */
			return {32'd0, 16'd0, pack(cqhdbl[(lv_index - 1) / 2].cqh)};
		     end
	       end
	       else
		  begin
				    /* no valid address specified -> return zero */
		     return 64'd0;
		  end
	    end
	 endcase
      endmethod: read_
   endinterface: ifc_config

   
   interface Ifc_completion ifc_completion;
      method Action _write(Bit#(`WDC) _data, Bit#(16) _tag);
	 /* Receive completions from PCIe. */
	 if (_tag == 'd1) begin
	    /* command completion received */
	    //$display("%d: Receiving Command", $stime());
	    // used in fetch state machine
	    rg_command_buf[rg_local_cmd_addr] <= _data;
	    
	    if (rg_local_cmd_addr ==
	       fromInteger(valueOf(TSub#(TDiv#(TMul#(
		  32, 16), `WDC), 1))))
	    begin
	       $display("%d: Last part received", $stime());
	       rg_local_cmd_addr <= 0;
	       // used in fetch state machine
	       drg_cmd_compl_received <= 1;
	    end
	 else
	    begin
	       // only used in this state
	       rg_local_cmd_addr <= rg_local_cmd_addr + 1;
	    end
	 end
	 else if (_tag == 'd2) begin
	    ff_prp_transfer_data.enq(_data);
	 end
	 else if (_tag > 2)
	    begin
	       /* data completion received; the tag indicates the channel number of
	       the nfc (nfc_id = tag - 2)*/
	       $display("%d: Try Receiving Data: 0x%x", $stime(), _data);
	       ff_data_transfer_data[_tag - 3].enq(_data);
	    end
      endmethod: _write
   endinterface: ifc_completion

   interface Ifc_ftl_processor_in ifc_ftl_processor_in;
      method Ftl_cmd get_cmd();
	 return wr_send_ftl_cmd;
      endmethod
      method Action _get_cmd_busy (bit _cmd_busy);
	 wr_ftl_cmd_busy <= unpack(_cmd_busy);
      endmethod
      method UInt#(64) get_prp();
	 return wr_send_ftl_prp;
      endmethod
      method Action _get_prp_busy (bit _prp_busy);
	 wr_ftl_prp_busy <= unpack(_prp_busy);
      endmethod
      method Action _get_meta_data_busy (bit _meta_data_busy);
      endmethod
   endinterface : ifc_ftl_processor_in
    
   interface ifc_ftl_processor_out = arr_ftl_proc_out_ifc;

   `ifdef DEBUG_ENABLE      
   method Bit#(8) leds_();
      return rg_out_leds2;
   endmethod: leds_
   `endif      

      /* interface definitions */
   interface nvmInterruptSideA_interface = fn_nvmInterruptSideA_ifc(
      vectr_rdy,
      vectr
      );

   interface nvmTransmitToPCIe_interface = fn_nvmTransmitToPCIe_interface(
      rg_out_data_to_pcie,
      rg_out_address_to_pcie,
      rg_out_tag_to_pcie,
      rg_out_write_to_pcie,
      rg_out_payload_length,
      drg_out_data_valid,
      dwr_wait
      );
    
endmodule
endpackage